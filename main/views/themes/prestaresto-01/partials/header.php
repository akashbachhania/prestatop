<?php
//echo $this->config->item('reservation_mode');die;
	$pages = $this->Pages_model->getPages();

	$body_class = '';
	if ($this->uri->rsegment(1) === 'menus') {
		$body_class = 'menus-page';
	}
?>
<?php echo get_doctype(); ?>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
	<head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php echo get_metas(); ?>
        <?php if ($favicon = get_theme_options('favicon')) { ?>
            <link href="<?php echo image_url($favicon); ?>" rel="shortcut icon" type="image/ico">
        <?php } else { ?>
            <?php echo get_favicon(); ?>
        <?php } ?>
        <title><?php echo sprintf(lang('site_title'), get_title(), config_item('site_name')); ?></title>
        <?php echo get_style_tags(); ?>
        <?php echo get_active_styles(); ?>
        <?php echo get_script_tags(); ?>
        <?php echo get_theme_options('ga_tracking_code'); ?>
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Varela" rel="stylesheet"> 
		<script type="text/javascript">
			var alert_close = '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';

			var js_site_url = function(str) {
				var strTmp = "<?php echo rtrim(site_url(), '/').'/'; ?>" + str;
			 	return strTmp;
			};

			var js_base_url = function(str) {
				var strTmp = "<?php echo base_url(); ?>" + str;
				return strTmp;
			};

            var pageHeight = $(window).height();

			$(document).ready(function() {
				if ($('#notification > p').length > 0) {
					setTimeout(function() {
						$('#notification > p').slideUp(function() {
							$('#notification').empty();
						});
					}, 3000);
				}

				$('.alert').alert();
				$('.dropdown-toggle').dropdown();
                $('a[title], i[title]').tooltip({placement: 'bottom'});
                
			});
		</script>
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
        <?php $custom_script = get_theme_options('custom_script'); ?>
        <?php if (!empty($custom_script['head'])) { echo '<script type="text/javascript">'.$custom_script['head'].'</script>'; }; ?>
	</head>
	<body class="<?php echo $body_class; ?>">
		<div id="opaclayer" onclick="closeReviewBox();"></div>
        <!--[if lt IE 7]>
            <p class="chromeframe"><?php echo lang('alert_info_outdated_browser'); ?></p>
        <![endif]-->

<style type="text/css">
header {
	left: 0;position: fixed;right: 0;top: 0;width: 100%;z-index: 1013;
}
/*body, #page-wrapper {
    background-color: #ffffff!important;
}*/
.header {
    background: rgba(0, 0, 0, 0) linear-gradient(to bottom, rgba(0, 0, 0, 0) 0%, transparent 100%) repeat scroll 0 0!important;
    height: 55px;
    left: 0;
    position: fixed;
    right: 0;
    top: 0;
    width: 100%;
    z-index: 1013;
}
.header--white.header {
    background: rgba(0, 0, 0, 0) none repeat scroll 0 0!important;
}
.header--white .header__language-switch {
    color: #d70f64!important;
    cursor: pointer;
    transition: color 0.1s linear 0s;
}

.background_img {
	top: -40px;
}
.header--white .header__wrapper__bg {
    box-shadow: 0 2px 4px -2px #4c4d4e;
    top: 0;
}
.header__wrapper__bg {
 height: 55px
     top: -55px;
}
.header__wrapper__bg {
    background-color: #fff!important;
    height: 55px;
    left: 0;
    position: absolute;
    top: -55px;
    transition: all 0.1s linear 0s;
    width: 100%;
    z-index: 1014;
}
.header__wrapper {
    height: 55px;
    margin-left: auto;
    margin-right: auto;
    max-width: 1170px;
    padding-left: 20px;
    padding-right: 20px;
}
.header__wrapper {
    padding-left: 20px;
    padding-right: 20px;
}
.header__wrapper {
    height: 46px;
    padding-left: 10px;
    padding-right: 10px;
    position: relative;
    z-index: 1015;
}
.header--white .header__language-switch {
    margin-top: 15px;
}
.header--white .header__language-switch {
    color: #d70f64;
    cursor: pointer;
    transition: color 0.1s linear 0s;
}
/*#main-header .navbar-nav > li > a {
       color: #fff;
       font-family: "Oxygen",Arial,sans-serif;
     transition: color 0.1s linear 0s;
  }
.backgroundhover {
       color: #d70f64!important;
       background-color: #fff!important;
  } */ 
 .navbar-nav > li > a {
    line-height: 14px;

} 
.backgroundhover {
  background-color:#FFF!important;
}
.backgroundhover_top {
  color:#FFF!important;
}

.backgroundhover_top{
 position:relative;
 text-decoration:none;
}

.backgroundhover_top:hover::after{
 content:"";
 position:absolute;
 left:30%;
 right:30%;
 bottom:0;
 border:1px solid white;
}

/*.dd:hover::after{
 content:"";
 position:absolute;
 left:30%;
 right:30%;
 bottom:0;
 border:1px solid #C30050!important;
}*/
.header__cart .fa-shopping-cart{
	font-size: 18px;
}

.menu__categories__list-item:focus{
	outline: none!important;

}
.header__cart {
    font-size: 2.4rem!important;
}
#main-header .navbar-nav > li > a {
    padding: 15px;
}
 #main-header .navbar-collapse {
    background-color: transparent;
}
/*.headernew {
	background-color: transparent!important;
}*/
/*#main-header .navbar-collapse {
 background: rgba(0, 0, 0, 0) linear-gradient(to bottom, rgba(0, 0, 0, 0.15) 0%, transparent 100%) repeat scroll 0 0;
}*/

@media screen and (max-width: 767px) and (min-width: 200px) {
    .container{
        padding-left: 0px;
        padding-right: 0px;
    }
}

@media screen and (max-width: 667px) {
    body {
        overflow-x: hidden !important;
    }
    .container {
        max-width: 100% !important;
        overflow-x: hidden !important;
    }
}

</style>
<script type="text/javascript">
	$(window).scroll(function() {
	if ($(this).scrollTop() > 1){  
    $('.scroll_image').attr('style', '');
    $('.normal_image').attr('style', 'display:none');
    $('header').addClass("header--white");
    $('.nav navbar-nav navbar-right li>a').addClass("header__language-switch");
    $('.logo a').addClass("header__language-switch");
    $(' #main-header .navbar-nav > li > a').addClass("backgroundhover");
    $(' #main-header .navbar-nav > li > a').removeClass("backgroundhover_top");
    $(' #main-header .navbar-nav > li > a').addClass("dd");

    //$(' #main-header .header__wrapper > .navbar-toggle').css("color",'#E76FA2');


 
     }
  else{
    //$('#main-header-menu-collapse').removeSt("background",'rgba(0, 0, 0, 0) linear-gradient(to bottom, rgba(0, 0, 0, 0.15) 0%, transparent 100%) repeat scroll 0 0');
    $('.scroll_image').attr('style', 'display:none');
    $('.normal_image').attr('style', '');

    $('header').removeClass("header--white");
    $('.nav navbar-nav navbar-right li>a').addClass("header__language-switch");
      $('.logo a').removeClass("header__language-switch");
    $(' #main-header .navbar-nav > li > a').removeClass("backgroundhover");
    $(' #main-header .navbar-nav > li > a').addClass("backgroundhover_top");
    $(' #main-header .navbar-nav > li > a').removeClass("dd");

    //$(' #main-header .header__wrapper > .navbar-toggle').css("color",'#fff');

  }
});
</script>
		<header id="main-header" class="header ">
			<div class="container">
                <div class="row">
		          <div class="header__wrapper__bg"></div>
                    <div class="col-sm-2 header__wrapper">
						
						<button aria-expanded="false" type="button" class="btn-navbar navbar-toggle collapsed hidden-xs" data-toggle="collapse" data-target="#main-header-menu-collapse">

						<i class="fa fa-align-justify"></i>
						</button>
                        <div class="logo header__logo">
                            <a class="header__logo__a" href="<?php echo rtrim(site_url(), '/').'/'; ?>">
								<?php if (get_theme_options('logo_image')) { ?>
									<img alt="<?php echo $this->config->item('site_name'); ?>" src="<?php echo image_url(get_theme_options('logo_image')) ?>" height="40">
								<?php } else if (get_theme_options('logo_text')) { ?>
									<?php echo get_theme_options('logo_text'); ?>
								<?php } else if ($this->config->item('site_logo') === 'data/no_photo.png') { ?>
									<?php echo $this->config->item('site_name'); ?>
								<?php } else { ?>
									<img class="normal_image hidden-xs" alt="<?php echo $this->config->item('site_name'); ?>" src="<?php echo image_url($this->config->item('site_logo')) ?>" height="40">

                                    <img class="scroll_image" style="display:none" alt="<?php echo $this->config->item('site_name'); ?>" src="<?php echo image_url($this->config->item('site_color_logo')) ?>" height="40">
								<?php } ?>
							</a>
						</div>
					</div>

                    <div class="col-sm-10 header__wrapper"> 						
 						<div class="header__account navbar-collapse collapse menulist headernew" id="main-header-menu-collapse" aria-expanded="false">
							
						</div>
					</div>
				</div>
			</div>
		</header>

		<div id="page-wrapper" class="content-area">
			<?php if (get_theme_options('display_crumbs') === '1' AND ($breadcrumbs = get_breadcrumbs()) !== '') { ?>
	            <div id="breadcrumb">
	                <div class="container">
	                    <div class="row">
	                        <div class="col-xs-12">
                                <?php echo $breadcrumbs; ?>
	                        </div>
	                    </div>
	                </div>
	            </div>
			<?php } ?>

            <?php if (($page_heading = get_heading()) !== '') { ?>
	            <div id="heading">
	                <div class="container">
	                    <div class="row">
	                        <div class="col-xs-12">
	                            <div class="heading-content">
	                                <h2><?php echo $page_heading; ?></h2>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
			<?php } ?>

<!-- <div class="modal-dialog info1" id="info" style="display:none;">
  
</div> -->
<style type="text/css">
.modal-close-button {
	border-radius: 50%;
	cursor: pointer;
	font-size: 16px;
	height: 33px;
	position: absolute;
	width: 35px;
	right: 15px;
	top: -19px;
}
.modal.fade .modal-dialog {
    transform: translate(0px, 0px);
    transition: transform 0.3s ease-out 0s;
}
</style>

  <!-- Modal -->
<div class="modal fade" id="info" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
<!--         <div class="modal-header">
            <div data-dismiss="modal" class="modal-close-button btn btn-primary">X</div>
        </div> -->
        <div data-dismiss="modal" class="modal-close-button btn btn-primary">X</div>
        <div class="modal-body info1">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>


<script type="text/javascript">
$('#info').modal('hide');
$(document).on('click','.top_url',function(){
	$.ajax({
	    url: "<?php echo base_url()?>Pages/get_info",
	    type: 'post',
	    data: 'page_id=' + $(this).attr('test'),
	        success: function(html) {
	        $('#info').modal('show');
	        $('.info1').html(html);
			},
	        error: function(xhr, ajaxOptions, thrownError) {
	            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	        }

	    });

});
</script>			

