<?php
	$pages = $this->Pages_model->getPages();

	$body_class = '';
	if ($this->uri->rsegment(1) === 'menus') {
		$body_class = 'menus-page';
	}
?>
<?php echo get_doctype(); ?>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
	<head>
        <?php echo get_metas(); ?>
        <?php if ($favicon = get_theme_options('favicon')) { ?>
            <link href="<?php echo image_url($favicon); ?>" rel="shortcut icon" type="image/ico">
        <?php } else { ?>
            <?php echo get_favicon(); ?>
        <?php } ?>
        <title><?php echo sprintf(lang('site_title'), get_title(), config_item('site_name')); ?></title>
        <?php echo get_style_tags(); ?>
        <?php echo get_active_styles(); ?>
        <?php echo get_script_tags(); ?>
        <?php echo get_theme_options('ga_tracking_code'); ?>
		<script type="text/javascript">
			var alert_close = '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';

			var js_site_url = function(str) {
				var strTmp = "<?php echo rtrim(site_url(), '/').'/'; ?>" + str;
			 	return strTmp;
			};

			var js_base_url = function(str) {
				var strTmp = "<?php echo base_url(); ?>" + str;
				return strTmp;
			};

            var pageHeight = $(window).height();

			$(document).ready(function() {
				if ($('#notification > p').length > 0) {
					setTimeout(function() {
						$('#notification > p').slideUp(function() {
							$('#notification').empty();
						});
					}, 3000);
				}

				$('.alert').alert();
				$('.dropdown-toggle').dropdown();
                $('a[title], i[title]').tooltip({placement: 'bottom'});
                $('select.form-control').select2();
			});
		</script>
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
        <?php $custom_script = get_theme_options('custom_script'); ?>
        <?php if (!empty($custom_script['head'])) { echo '<script type="text/javascript">'.$custom_script['head'].'</script>'; }; ?>
	</head>
	<body class="<?php echo $body_class; ?>">
		<div id="opaclayer" onclick="closeReviewBox();"></div>
        <!--[if lt IE 7]>
            <p class="chromeframe"><?php echo lang('alert_info_outdated_browser'); ?></p>
        <![endif]-->

<style type="text/css">
header {
	left: 0;position: fixed;right: 0;top: 0;width: 100%;z-index: 1013;
}
/*body, #page-wrapper {
    background-color: #ffffff!important;
}*/
.header {
    background: rgba(0, 0, 0, 0) linear-gradient(to bottom, rgba(0, 0, 0, 0.65) 0%, transparent 100%) repeat scroll 0 0!important;
    height: 55px;
    left: 0;
    position: fixed;
    right: 0;
    top: 0;
    width: 100%;
    z-index: 1013;
}
.header--white.header {
    background: rgba(0, 0, 0, 0) none repeat scroll 0 0!important;
}
.header--white .header__language-switch {
    color: #d70f64!important;
    cursor: pointer;
    transition: color 0.1s linear 0s;
}

.background_img {
	top: -40px;
}
.header--white .header__wrapper__bg {
    border-bottom: 1px solid #bcbcbc;
    top: 0;
}
.header__wrapper__bg {
 height: 55px
     top: -55px;
}
.header__wrapper__bg {
    background-color: #fff!important;
    height: 55px;
    left: 0;
    position: absolute;
    top: -55px;
    transition: all 0.1s linear 0s;
    width: 100%;
    z-index: 1014;
}
.header__wrapper {
    height: 55px;
    margin-left: auto;
    margin-right: auto;
    max-width: 1170px;
    padding-left: 20px;
    padding-right: 20px;
}
.header__wrapper {
    padding-left: 20px;
    padding-right: 20px;
}
.header__wrapper {
    height: 46px;
    padding-left: 10px;
    padding-right: 10px;
    position: relative;
    z-index: 1015;
}
.header--white .header__language-switch {
    margin-top: 15px;
}
.header--white .header__language-switch {
    color: #d70f64;
    cursor: pointer;
    transition: color 0.1s linear 0s;
}
/*#main-header .navbar-nav > li > a {
       color: #fff;
       font-family: "Oxygen",Arial,sans-serif;
     transition: color 0.1s linear 0s;
  }
.backgroundhover {
       color: #d70f64!important;
       background-color: #fff!important;
  } */ 
 .navbar-nav > li > a {
    line-height: 14px;

} 
.backgroundhover {
  background-color:#FFF!important;
}
.backgroundhover_top {
  color:#FFF!important;
}


.header__cart {
    font-size: 2.4rem!important;
}
#main-header .navbar-nav > li > a {
    padding: 15px;
}
#main-header .navbar-collapse {
 background: rgba(0, 0, 0, 0) linear-gradient(to bottom, rgba(0, 0, 0, 0.15) 0%, transparent 100%) repeat scroll 0 0;
}
</style>
<script type="text/javascript">
	$(window).scroll(function() {
	if ($(this).scrollTop() > 1){  
 
    $('#main-header-menu-collapse').css("background",'#fff');

    $('header').addClass("header--white");
    $('.nav navbar-nav navbar-right li>a').addClass("header__language-switch");
    $('.logo a').addClass("header__language-switch");
    $(' #main-header .navbar-nav > li > a').addClass("backgroundhover");
  $(' #main-header .navbar-nav > li > a').removeClass("backgroundhover_top");

    //$(' #main-header .header__wrapper > .navbar-toggle').css("color",'#E76FA2');


 
     }
  else{
    $('#main-header-menu-collapse').css("background",'rgba(0, 0, 0, 0) linear-gradient(to bottom, rgba(0, 0, 0, 0.15) 0%, transparent 100%) repeat scroll 0 0');
    $('header').removeClass("header--white");
    $('.nav navbar-nav navbar-right li>a').addClass("header__language-switch");
      $('.logo a').removeClass("header__language-switch");
    $(' #main-header .navbar-nav > li > a').removeClass("backgroundhover");
    $(' #main-header .navbar-nav > li > a').addClass("backgroundhover_top");
    //$(' #main-header .header__wrapper > .navbar-toggle').css("color",'#fff');

  }
});
</script>
		<header id="main-header" class="header">
			<div class="container">
                <div class="row">
		<div class="header__wrapper__bg"></div>
                    <div class="col-sm-2 header__wrapper">
						
						<button aria-expanded="false" type="button" class="btn-navbar navbar-toggle collapsed" data-toggle="collapse" data-target="#main-header-menu-collapse">

						<i class="fa fa-align-justify"></i>
						</button>
                        <div class="logo header__logo">
                            <a class="header__logo__a" href="<?php echo rtrim(site_url(), '/').'/'; ?>">
								<?php if (get_theme_options('logo_image')) { ?>
									<img alt="<?php echo $this->config->item('site_name'); ?>" src="<?php echo image_url(get_theme_options('logo_image')) ?>" height="40">
								<?php } else if (get_theme_options('logo_text')) { ?>
									<?php echo get_theme_options('logo_text'); ?>
								<?php } else if ($this->config->item('site_logo') === 'data/no_photo.png') { ?>
									<?php echo $this->config->item('site_name'); ?>
								<?php } else { ?>
									<img alt="<?php echo $this->config->item('site_name'); ?>" src="<?php echo image_url($this->config->item('site_logo')) ?>" height="40">
								<?php } ?>
							</a>
						</div>
					</div>

                    <div class="col-sm-10 header__wrapper"> 						
 						<div class="header__account navbar-collapse collapse menulist" id="main-header-menu-collapse" aria-expanded="false">
							<ul class="nav navbar-nav navbar-right">

								<?php if (!empty($pages)) { ?>
									<?php foreach ($pages as $page) { ?>
										<?php if (is_array($page['navigation']) AND in_array('header', $page['navigation'])) { ?>
											<li><a class="backgroundhover_top" href="<?php echo site_url('pages?page_id='.$page['page_id']); ?>"><?php echo $page['name']; ?></a></li>
										<?php } ?>
									<?php } ?>
								<?php } ?>

<!-- 
									<li><a href="#">LINK#1</a></li>
									<li><a href="#">BLOC MENU</a></li>
									<li><a href="#">LINK#2</a></li>
 -->
								<?php  if ($this->config->item('reservation_mode') === '1') { ?>
									<li><a class="backgroundhover_top" href="<?php echo site_url('reservation'); ?>"><?php echo lang('menu_reservation'); ?></a></li>
								<?php } ?>
								<li>
									<a class="backgroundhover_top header__cart" href="#">
									<i class="fa fa-shopping-cart"></i></a>
								</li>

								<?php if ($this->customer->isLogged()) { ?>
									<li class="dropdown"><a class="dropdown-toggle clickable" data-toggle="dropdown" id="dropdownLabel1"><?php echo lang('menu_my_account'); ?> <span class="caret"></span></a>
										<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownLabel1">
                                            <li><a role="presentation" href="<?php echo site_url('account/orders'); ?>"><?php echo lang('menu_recent_order'); ?></a></li>
                                            <li><a role="presentation" href="<?php echo site_url('account/account'); ?>"><?php echo lang('menu_my_account'); ?></a></li>
                                            <li><a role="presentation" href="<?php echo site_url('account/address'); ?>"><?php echo lang('menu_address'); ?></a></li>

											<?php if ($this->config->item('reservation_mode') === '1') { ?>
												<li><a role="presentation" href="<?php echo site_url('account/reservations'); ?>"><?php echo lang('menu_recent_reservation'); ?></a></li>
											<?php } ?>

											<li><a role="presentation" href="<?php echo site_url('account/logout'); ?>" ><?php echo lang('menu_logout'); ?></a></li>
										</ul>
									</li>
								<?php } else { ?>
									<li><a class="backgroundhover_top show-login-modal header__account__login-text" href="<?php echo site_url('account/login'); ?>"><?php echo lang('menu_login'); ?></a></li>
									<?php ?><li><a class="backgroundhover_top show-login-modal header__account__login-text" href="<?php echo site_url('account/register'); ?>"><?php echo lang('menu_register'); ?></a></li> <?php ?>
								<?php } ?>

							</ul>
						</div>
					</div>
				</div>
			</div>
		</header>

		<div id="page-wrapper" class="content-area">
			<?php if (get_theme_options('display_crumbs') === '1' AND ($breadcrumbs = get_breadcrumbs()) !== '') { ?>
	            <div id="breadcrumb">
	                <div class="container">
	                    <div class="row">
	                        <div class="col-xs-12">
                                <?php echo $breadcrumbs; ?>
	                        </div>
	                    </div>
	                </div>
	            </div>
			<?php } ?>

            <?php if (($page_heading = get_heading()) !== '') { ?>
	            <div id="heading">
	                <div class="container">
	                    <div class="row">
	                        <div class="col-xs-12">
	                            <div class="heading-content">
	                                <h2><?php echo $page_heading; ?></h2>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
			<?php } ?>