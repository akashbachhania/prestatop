</div>
</div>
<footer id="page-footer">
    <?php echo get_partial('content_footer'); ?>


<!--     <div class="bottom-footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 wrap-all border-top">
                </div>
            </div>
        </div>
	</div> -->
</footer>

<div class="container-fluid copyright">
    <div class="container">
        <div class="inner-copyright">
        <?php echo sprintf(lang('site_copyright'), date('Y'), config_item('site_name'), lang('6')) . lang('6'); ?> <a href="http://#">Powered by Prestaresto</a>
        </div>
    </div>
</div>


<!--  <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAuAHWhfmobrYCnvEURs2z32HU7YpB_W5A&libraries=places"></script> -->


<script type="text/javascript">


//get value from select field and set responce in time slot
$(document).on('change','.day_select',function() { 
console.log($('.day_select :selected').val());
$('.addressvalidation').hide();
    if($('.day_select :selected').val()=='')
    {
        $('.delivery_time').html('');
    }else {
     
        $.ajax({
            url: js_site_url('cart_module/cart_module/daytimeslot'),
            type: 'post',
            data:'day=' +  $('.day_select :selected').text()+'&order_type=' + $('#popup_order_type').val(),
            success: function(html) { 
                $('.delivery_time').html(html);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }

        });
    }    

});

 
function initialize() {

 var options = {
  types: ['geocode'],
  componentRestrictions: {country: "fr"}
 };

 var input = document.getElementById('searchTextField');

 var autocomplete = new google.maps.places.Autocomplete(input, options);
}
   google.maps.event.addDomListener(window, 'load', initialize);
</script>


<?php $custom_script = get_theme_options('custom_script'); ?>
<?php if (!empty($custom_script['footer'])) { echo '<script type="text/javascript">'.$custom_script['footer'].'</script>'; }; ?>
</body>
</html>



