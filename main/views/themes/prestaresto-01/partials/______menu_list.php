<style type="text/css">

.menu__items__title, .menu__bottom-info__openings-title {
    font-size: 1.4rem;
    padding: 55px 0 25px;
}
.menu__items__title, .menu__bottom-info__openings-title {
    border-bottom: 1px solid #bcbcbc;
    font-family: "MuseoSans-700",Arial,sans-serif;
    font-size: 1.2rem;
    line-height: normal;
    margin: 0;
    padding: 35px 0 15px;
    text-transform: uppercase;
}




.menu__items__title:first-child, .menu__bottom-info__openings-title:first-child {
    padding-top: 40px;
}
.menu__items__title, .menu__bottom-info__openings-title {
    font-size: 1.4rem;
    padding: 55px 0 25px;
}
.menu__items__title, .menu__bottom-info__openings-title {
    border-bottom: 1px solid #bcbcbc;
    font-family: "MuseoSans-700",Arial,sans-serif;
    font-size: 1.2rem;
    line-height: normal;
    margin: 0;
    padding: 35px 0 15px;
    text-transform: uppercase;
}
.text-center, .home__stats__comment, .restaurants__city-bottom-info__description h3, .restaurants__city-bottom-info__description .h3, .menu__items__title, .menu__bottom-info__openings-title, .desktop-cart__title, .desktop-cart_order__message, .desktop-cart__footer__message {
    text-align: center;
}
.menu__items-wrapper {
    float: left;
    width: 75%;
}
.menu__items-wrapper {
    float: left;
    width: 100%;
}
.menu__items-wrapper {
    min-height: 1px;
    padding-bottom: 15px;
    padding-left: 10px;
    padding-right: 10px;
    position: relative;
}
.menu__item__name {
    font-weight: bold;
    padding: 5px 0px;
    text-transform: capitalize;
}
.menu__item__description {
    text-align: left;
    font-size: 13px;
    line-height: 18px;
}
.menu__item:hover {
	cursor: pointer; 
}
.menu-item:hover {
	background-color: #F7F7F7;

}
.menu-item {
    border-bottom: 1px solid #bcbcbc;
    padding-bottom: 30px;

}

.menu__item {
    font-size: 1.4rem;
    min-height: 96px;
}
.menu__item {
/*    border-bottom: 1px solid #bcbcbc;*/
    display: table;
    float: left;
    font-size: 1.4rem;
    line-height: normal;
    min-height: 60px;
/*    padding: 0 5px 10px 10px;*/
    transition: background-color 0.1s linear 0s;
    width: 100%;
}
.menu__item__price {
    padding-left: 0;
}
.menu__item__price {
    display: table-cell;
    padding-left: 10px;
    text-align: right;
   
}
.menu__item__price-button {
    background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
    border: medium none;
    height: 20px;
    padding: 0 5px;
}
input, button, select, textarea {
    font-family: inherit;
    font-size: inherit;
    line-height: inherit;
}
button, html input[type="button"], input[type="reset"], input[type="submit"] {
    cursor: pointer;
}
button, select {
    text-transform: none;
}
button {
    overflow: visible;
}
button, input, optgroup, select, textarea {
    color: inherit;
    font: inherit;
    margin: 0;
}

.icon-plus::before {
    content: "+";
}
.menu__item__price-button-icon {
    font-size: 1.3rem;
    font-weight: 100;
    margin-top: -1px;
    vertical-align: middle;
}
.menu-item:hover .menu__item__price-button-icon {
    font-size: 1.9rem;
    margin-top: -1px;
    vertical-align: middle;
    font-weight: bolder;
    margin-top: -1px;
    margin-right:-5px;
    font-weight: 700;
    color:<?php echo $button_info['link']['hover']?>!important;;

}

[class^="icon-"], [class*=" icon-"] {
    line-height: 1em;
}
</style>	
<?php
 if ($categories) { ?>
	<div id="Container" class="menu__items-wrapper">
		<?php $category_count = 1; ?>
		<?php foreach ($categories as $category_id => $category) { ?>
			<?php $category_name = strtolower(str_replace(' ', '-', str_replace('&', '_', $category['name']))); ?>
			<div class="menu__items menu-container mix <?php echo $category_name; ?>">
				<a class="menu-toggle visible-xs visible-sm collapsed" href="#<?php echo $category_name; ?>" role="button" data-toggle="collapse" data-parent=".menu-list" aria-expanded="<?php echo ($category_count === 1) ? 'true' : 'false'; ?>" aria-controls="<?php echo $category_name; ?>">
					<?php echo $category['name']; ?>
					<i class="fa fa-angle-down fa-2x fa-pull-right text-muted"></i>
					<i class="fa fa-angle-up fa-2x fa-pull-right text-muted"></i>
				</a>
				<div id="<?php echo strtolower($category_name); ?>" class="navbar-collapse collapse <?php echo ($category_count === 1) ? 'in' : ''; ?> wrap-none">
					<div class="menu-category">
						<h3 class="menu__items__title"><?php echo $category['name']; ?></h3>
						<!--<p><?php //echo $category['description']; ?><!--</p>-->
						<?php /*if (!empty($category['image'])) { ?>
							<img class="img-responsive" src="<?php echo $category['image']; ?>" alt="<?php echo $category['name']; ?>"/>
						<?php }*/?>
					</div>

					 <div class="menu__item">
						<?php if (isset($menus[$category_id]) AND !empty($menus[$category_id])) { ?>
							<?php foreach ($menus[$category_id] as $menu) {
							 ?>

								<div id="menu<?php echo $menu['menu_id']; ?>" class="menu-item" onClick="openMenuOptions('<?php echo $menu['menu_id']; ?>', '<?php echo $menu['minimum_qty']; ?>');">
									<div class="menu__item__wrapper">
										<?php/* if ($show_menu_images === '1' AND !empty($menu['menu_photo'])) { ?>
											<div class="menu-thumb col-xs-2 col-sm-2 wrap-none wrap-right">
												<img class="img-responsive img-thumbnail" alt="<?php echo $menu['menu_name']; ?>" src="<?php echo $menu['menu_photo']; ?>">
											</div>
										<?php }*/ ?>

										<div class="menu-content col-md-9 col-sm-10 col-xs-10 wrap-none wrap-right">

											<div class="menu__item__name">
											<span class="shopping">
												<?php

												//echo '<pre>';print_r($_SESSION['cart_contents']);die;
												if(isset($_SESSION['cart_contents']) AND !empty($_SESSION['cart_contents'])){
													foreach ($_SESSION['cart_contents'] as $key => $value) {
														if($key=='cart_total' || $key=='total_items' || $key=='order_total' || $key=='totals')
															continue;
														if($value['id']==$menu['menu_id'])
														{ ?>
															<i class="fa fa-shopping-cart"></i>
	 													<?php break; }	
													}
												} ?>
												</span>
													<b><?php echo $menu['menu_name']; ?>
														
													</b>
												
											</div>
											<div class="menu__item__description">
												<?php echo $menu['menu_description']; ?>
											</div>
										</div>
										<div class="col-md-3 col-sm-2 col-xs-2 menu__item__price">
											<?php echo $menu['menu_price']; ?>

											<?php if ($menu['mealtime_status'] === '1' AND empty($menu['is_mealtime'])) { ?>
													<a class="menu__item__price-button btn-cart add_cart disabled">												<i class="fa fa-plus menu__item__price-button-icon" aria-hidden="true"></i>
													</a>
											<?php } else { ?>
												<span class="add_plus">	
													<a class="menu__item__price-button btn-cart add_cart">
													<?php
/*													if(isset($_SESSION['cart_contents']) AND !empty($_SESSION['cart_contents'])){
													$var=0;
														foreach ($_SESSION['cart_contents'] as $key => $value) {
															if($key=='cart_total' || $key=='total_items' || $key=='order_total' || $key=='totals')
																continue;
															if($value['id']==$menu['menu_id'])
															{ $var=1;break; }	
														}
													if($var==0)
													{?>

														<i class="menu__item__price-button-icon icon-plus"></i>
													<?php }	

													} else{ ?>
													<?php }*/?>
														
														<i class="fa fa-plus menu__item__price-button-icon" aria-hidden="true"></i>

													</a>
												</span>	
												<?php } ?>

<!--<i class="menu__item__price-button-icon icon-plus"></i>-->


										</div>

										<!--<div class="menu-right col-md-4 wrap-none">


											<span class="menu-price">
											<?/*php echo $menu['menu_price']; ?>
											</span>
											<span class="menu-button">
												<?php if ($menu['mealtime_status'] === '1' AND empty($menu['is_mealtime'])) { ?>
													<a class="btn btn-primary btn-cart add_cart disabled"><span class="fa fa-plus"></span></a>
												<?php } else if (isset($menu_options[$menu['menu_id']])) { ?>
													<a class="btn btn-primary btn-cart add_cart" onClick="openMenuOptions('<?php echo $menu['menu_id']; ?>', '<?php echo $menu['minimum_qty']; ?>');">
														<span class="fa fa-plus"></span>
													</a>
												<?php } else { ?>
													<a class="btn btn-primary btn-cart add_cart" onClick="addToCart('<?php echo $menu['menu_id']; ?>', '<?php echo $menu['minimum_qty']; ?>');">
														<span class="fa fa-plus"></span>
													</a>
												<?php } ?>
											</span>
											<?php if ($menu['mealtime_status'] === '1' AND empty($menu['is_mealtime'])) { ?>
												<div class="menu-mealtime text-danger"><?php echo sprintf(lang('text_mealtime'), $menu['mealtime_name'], $menu['start_time'], $menu['end_time']); ?></div>
											<?php }?>

											<?php if ($menu['special_status'] === '1' AND $menu['is_special'] === '1') { ?>
												<div class="menu-special"><?php echo $menu['end_days']; ?></div>
											<?php }*/?>
										<!--</div>-->
										
									</div>
								</div>
							<?php } ?>
						<div class="gap"></div>
						<div class="gap"></div>
					 							

						<?php } else { ?>
							<p style="margin-top:10px"><?php echo lang('text_empty'); ?></p>
						<?php } ?>

					 </div>

				</div>
			</div>
			<?php $category_count++; ?>
		<?php } ?>
	</div>
<?php } else { ?>
	<p><?php echo lang('text_no_category'); ?></p>
<?php } ?>

<?php if (!empty($menu_total) AND $menu_total < 150) { ?>
	<div class="pager-list"></div>
<?php } else { ?>
	<div class="pagination-bar text-right">
		<div class="links"><?php echo $pagination['links']; ?></div>
		<div class="info"><?php echo $pagination['info']; ?></div>
	</div>
<?php } ?>

<script type="text/javascript">
	$(document).on('click','.menu-toggle',function(){
		var current_class=$(this).attr('aria-controls');
		$('.'+current_class+' div').addClass('in');
	});
</script>