</div>
</div>
<footer id="page-footer">
    <?php echo get_partial('content_footer'); ?>

    <div class="main-footer">
	    <div class="container">
            <div class="row">
                <div id="local-alert" class="<?php echo ($location_search === TRUE) ? 'col-xs-9 col-sm-6 center-block' : 'col-sm-12' ?>">
                    <div class="local-alert"></div>
                    <?php if (!empty($local_alert)) { ?>
                        <?php echo $local_alert; ?>
                    <?php } ?>
                </div>

                <?php if ($location_search !== TRUE AND $rsegment !== 'locations') { ?>
                    <div id="local-info" class="col-md-12" style="display: <?php echo ($local_info) ? 'block' : 'none'; ?>">
                        <div class="panel panel-local display-local col-md-12">
                            <?php /* if ($location_search_mode === 'multi') { ?>
                                <div class="panel-heading">
                                    <div class="row local-search bg-warning" style="display: <?php echo (empty($search_query) AND $location_order === '1') ? 'block' : 'none'; ?>">
                                        <a class="close-search clickable" onclick="toggleLocalSearch();">&times;</a>
                                        <div class="col-xs-12 col-sm-6 center-block">
                                            <div class="postcode-group text-center">
                                                <?php echo lang('text_no_search_query'); ?>&nbsp;&nbsp;&nbsp;&nbsp;
                                                <div class="input-group">
                                                    <input type="text" id="search-query" class="form-control text-center postcode-control input-xs" name="search_query" placeholder="<?php echo lang('label_search_query'); ?>" value="<?php echo $search_query; ?>">
                                                    <a id="search" class="input-group-addon btn btn-primary" onclick="searchLocal();"><?php echo lang('button_search_location'); ?></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row local-change" style="display: <?php echo (!empty($search_query) OR (empty($search_query) AND $location_order !== '1')) ? 'block' : 'none'; ?>">
                                        <div class="col-xs-12 col-sm-7">
                                            <?php $text_location_summary = ($has_search_query AND $delivery_coverage) ? lang('text_location_summary') : lang('text_delivery_coverage'); ?>
                                            <?php $text_search_query = (empty($search_query)) ? lang('text_enter_location') : sprintf($text_location_summary, lang('text_at').$search_query); ?>
                                            <?php echo $text_search_query; ?>&nbsp;&nbsp;
                                            <a onclick="toggleLocalSearch();" class="clickable btn-link visible-xs-block visible-sm-inline-block visible-md-inline-block visible-lg-inline-block" title="">
                                                <?php echo empty($search_query) ? lang('button_enter_location') : lang('button_change_location'); ?>
                                            </a>
                                        </div>

                                        <?php if (!in_array($rsegment, array('local', 'locations'))) { ?>
                                            <div class="col-xs-12 col-sm-5 text-right">
                                                <a class="btn btn-primary btn-menus" href="<?php echo site_url('local?location_id='.$location_id).'#local-menus'; ?>"><i class="fa fa-cutlery"></i>
                                                    <span>&nbsp;&nbsp;<?php echo lang('text_goto_menus'); ?></span>
                                                </a>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php }*/ ?>

                            <div class="panel-body">
                                <div class="row boxes">
                                    <div class="box-one col-xs-12 col-sm-5 col-md-5">
                                        <?php if (!empty($location_image)) { ?>
                                            <img class="img-responsive pull-left" src="<?php echo $location_image; ?>">
                                        <?php } ?>
                                        <dl <?php echo (!empty($location_image)) ? 'class="box-image"' : ''; ?>>
                                            <dd><h4><?php echo $location_name; ?></h4></dd>
                                            <?php if (config_item('allow_reviews') !== '1') { ?>
                                                <dd class="text-muted">
                                                    <div class="rating rating-sm">
                                                        <span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star-half-o"></span><span class="fa fa-star-o"></span>
                                                        <span class="small"><?php echo $text_total_review; ?></span>
                                                    </div>
                                                </dd>
                                            <?php } ?>
                                            <dd><span class="text-muted"><?php echo str_replace('<br />', ', ', $location_address); ?></span></dd>
                                        </dl>
                                    </div>
                                    <div class="col-xs-12 box-divider visible-xs"></div>
                                    <div class="box-two col-xs-12 col-sm-3 col-md-3">
                                        <dl>
                                            <?php if ($opening_status === 'open') { ?>
                                                <dt><?php echo lang('text_is_opened'); ?></dt>
                                            <?php } else if ($opening_status === 'opening') { ?>
                                                <dt class="text-muted"><?php echo sprintf(lang('text_opening_time'), $opening_time); ?></dt>
                                            <?php } else { ?>
                                                <dt class="text-muted"><?php echo lang('text_closed'); ?></dt>
                                            <?php } ?>
                                            <?php if ($opening_status !== 'closed') { ?>
                                                <dd class="visible-xs">
                                                    <?php if (!empty($opening_type) AND $opening_type == '24_7') { ?>
                                                        <span class="fa fa-clock-o"></span>&nbsp;&nbsp;<span><?php echo lang('text_24_7_hour'); ?></span>
                                                    <?php } else if (!empty($opening_time) AND !empty($closing_time)) { ?>
                                                        <span class="fa fa-clock-o"></span>&nbsp;&nbsp;<span><?php echo $opening_time; ?> - <?php echo $closing_time; ?></span>
                                                    <?php } ?>
                                                </dd>
                                            <?php } ?>
                                            <dd class="text-muted">
                                                <?php if ($has_delivery) { ?>
                                                    <?php if ($delivery_status === 'open') { ?>
                                                        <?php echo sprintf(lang('text_delivery_time_info'), sprintf(lang('text_in_minutes'), $delivery_time)); ?>
                                                    <?php } else if ($delivery_status === 'opening') { ?>
                                                        <?php echo sprintf(lang('text_delivery_time_info'), sprintf(lang('text_starts'), $delivery_time)); ?>
                                                    <?php } else { ?>
                                                        <?php echo sprintf(lang('text_delivery_time_info'), lang('text_is_closed')); ?>
                                                    <?php } ?>
                                                <?php } ?>
                                            </dd>
                                            <dd class="text-muted">
                                                <?php if ($has_collection) { ?>
                                                    <?php if ($collection_status === 'open') { ?>
                                                        <?php echo sprintf(lang('text_collection_time_info'), sprintf(lang('text_in_minutes'), $collection_time)); ?>
                                                    <?php } else if ($collection_status === 'opening') { ?>
                                                        <?php echo sprintf(lang('text_collection_time_info'), sprintf(lang('text_starts'), $collection_time)); ?>
                                                    <?php } else { ?>
                                                        <?php echo sprintf(lang('text_collection_time_info'), lang('text_is_closed')); ?>
                                                    <?php } ?>
                                                <?php } ?>
                                            </dd>
                                        </dl>
                                    </div>
                                    <div class="col-xs-12 box-divider visible-xs"></div>
                                    <div class="box-three col-xs-12 col-sm-4 col-md-4">
                                        <dl>
                                            <?php if ($opening_status !== 'closed') { ?>
                                                <dd class="hidden-xs">
                                                    <?php if (!empty($opening_type) AND $opening_type == '24_7') { ?>
                                                        <span class="fa fa-clock-o"></span>&nbsp;&nbsp;<span><?php echo lang('text_24_7_hour'); ?></span>
                                                    <?php } else if (!empty($opening_time) AND !empty($closing_time)) { ?>
                                                        <span class="fa fa-clock-o"></span>&nbsp;&nbsp;<span><?php echo $opening_time; ?> - <?php echo $closing_time; ?></span>
                                                    <?php } ?>
                                                </dd>
                                            <?php } ?>
                                            <dd class="text-muted">
                                                <?php if (!$has_delivery AND $has_collection) { ?>
                                                    <?php echo lang('text_collection_only'); ?>
                                                <?php } else if ($has_delivery AND !$has_collection) { ?>
                                                    <?php echo lang('text_delivery_only'); ?>
                                                <?php } else if ($has_delivery AND $has_collection) { ?>
                                                    <?php echo lang('text_both_types'); ?>
                                                <?php } else { ?>
                                                    <?php echo lang('text_no_types'); ?>
                                                <?php } ?>
                                            </dd>
                                            <?php if ($has_delivery) { ?>
                                                <dd class="text-muted"><?php echo $text_delivery_condition; ?></dd>
                                                
                                            <?php } ?>
                                        </dl>
                                   </div>
                                </div>
                                                    
                            <div class="menu-container1">
                                <div class="navbar-collapse collapse  wrap-none">
                                    <div class="menu-category1">
                                        <h3 class="menu__items__title">&nbsp;</h3>
                                        <p>
                                            
                                        </p>
                                    </div>

                                     <div class="menu__item">
                                        <p>
                                         <?php $social_icons = get_theme_options('social'); ?>

                                         <div class="col-md-6">
                                            <div class="social-icons">
                                            <?php if (!empty($social_icons['facebook'])) { ?>
                                    <a class="fa fa-facebook" target="_blank" href="<?php echo $social_icons['facebook']; ?>"></a>
                                <?php } ?>

                                <?php if (!empty($social_icons['twitter'])) { ?>
                                    <a class="fa fa-twitter" target="_blank" href="<?php echo $social_icons['twitter']; ?>"></a>
                                <?php } ?>

                                <?php if (!empty($social_icons['google'])) { ?>
                                    <a class="fa fa-google-plus" target="_blank" href="<?php echo $social_icons['google']; ?>"></a>
                                <?php } ?>

                                <?php if (!empty($social_icons['youtube'])) { ?>
                                    <a class="fa fa-youtube" target="_blank" href="<?php echo $social_icons['youtube']; ?>"></a>
                                <?php } ?>

                                <?php if (!empty($social_icons['vimeo'])) { ?>
                                    <a class="fa fa-vimeo" target="_blank" href="<?php echo $social_icons['vimeo']; ?>"></a>
                                <?php } ?>

                                <?php if (!empty($social_icons['linkedin'])) { ?>
                                    <a class="fa fa-linkedin" target="_blank" href="<?php echo $social_icons['linkedin']; ?>"></a>
                                <?php } ?>

                                <?php if (!empty($social_icons['pinterest'])) { ?>
                                    <a class="fa fa-pinterest" target="_blank" href="<?php echo $social_icons['pinterest']; ?>"></a>
                                <?php } ?>

                                <?php if (!empty($social_icons['tumblr'])) { ?>
                                    <a class="fa fa-tumblr" target="_blank" href="<?php echo $social_icons['tumblr']; ?>"></a>
                                <?php } ?>

                                <?php if (!empty($social_icons['flickr'])) { ?>
                                    <a class="fa fa-flickr" target="_blank" href="<?php echo $social_icons['flickr']; ?>"></a>
                                <?php } ?>

                                <?php if (!empty($social_icons['instagram'])) { ?>
                                    <a class="fa fa-instagram" target="_blank" href="<?php echo $social_icons['instagram']; ?>"></a>
                                <?php } ?>

                                <?php if (!empty($social_icons['dribbble'])) { ?>
                                    <a class="fa fa-dribbble" target="_blank" href="<?php echo $social_icons['dribbble']; ?>"></a>
                                <?php } ?>

                                <?php if (!empty($social_icons['foursquare'])) { ?>
                                    <a class="fa fa-foursquare" target="_blank" href="<?php echo $social_icons['foursquare']; ?>"></a>
                                <?php } ?>

                                            <?php //echo sprintf(lang('site_copyright'), date('Y'), config_item('site_name'), lang('tastyigniter_system_name')) . lang('tastyigniter_system_powered'); ?>
                                        </div>
                                    </div>
                                        <div class="col-md-6" align="right">
                                       <?php $pages = $this->Pages_model->getPages(); ?>
                                        <?php if ($pages) { ?>
                                            <?php foreach ($pages as $page) { ?>
                                                <?php if (in_array('footer', $page['navigation'])) { ?>
                                                    <a class="footer_link" href="<?php echo site_url('pages?page_id='.$page['page_id']); ?>"><?php echo $page['name']; ?></a>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?> 

                                        </p>                                   
                                        <div class="gap"></div>
                                        <div class="gap"></div>




                                     </div>
                                </div>
                            </div>
                        </div>    
                    </div>
                    <?php $social_icons = get_theme_options('social'); ?>
                    <?php if (!empty($social_icons) AND array_filter($social_icons)) { ?>
                        <div class="social-bottom ">

                    <div class="col-md-5"></div>
                            <div class="social-icons">
                                <!-- Removed social icons from here and put upper-->
                            
                        </div>
                    <?php } ?>
                        </div>
                <?php } ?>

            </div>
        </div>


    </div>

<!--     <div class="bottom-footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 wrap-all border-top">
                </div>
            </div>
        </div>
	</div> -->
</footer>

<div class="container-fluid copyright">
    <div class="container">
        <div class="inner-copyright">
        <?php echo sprintf(lang('site_copyright'), date('Y'), config_item('site_name'), lang('tastyigniter_system_name')) . lang('tastyigniter_system_powered'); ?>
        </div>
    </div>
</div>


<!--  <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAuAHWhfmobrYCnvEURs2z32HU7YpB_W5A&libraries=places"></script> -->


<script type="text/javascript">


//get value from select field and set responce in time slot
$(document).on('change','.day_select',function() { 
console.log($('.day_select :selected').val());
$('.addressvalidation').hide();
    if($('.day_select :selected').val()=='')
    {
        $('.delivery_time').html('');
    }else {
     
        $.ajax({
            url: js_site_url('cart_module/cart_module/daytimeslot'),
            type: 'post',
            data:'day=' +  $('.day_select :selected').text()+'&order_type=' + $('#popup_order_type').val(),
            success: function(html) { 
                $('.delivery_time').html(html);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }

        });
    }    

});

 
function initialize() {

 var options = {
  types: ['geocode'],
  componentRestrictions: {country: "fr"}
 };

 var input = document.getElementById('searchTextField');

 var autocomplete = new google.maps.places.Autocomplete(input, options);
}
   google.maps.event.addDomListener(window, 'load', initialize);
</script>


<?php $custom_script = get_theme_options('custom_script'); ?>
<?php if (!empty($custom_script['footer'])) { echo '<script type="text/javascript">'.$custom_script['footer'].'</script>'; }; ?>
</body>
</html>



