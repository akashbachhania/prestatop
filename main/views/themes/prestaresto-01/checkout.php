<style type="text/css">
	.div_border {
		border-bottom: 1px solid #bcbcbc;
	}
	.select2-drop-active {
		background-color: #fff!important;
		border-color: #df0f49;
		color:#686868!important;
	}
/*	.my_fields:hover {
		background-color: #fff!important;
		border-color: #df0f49;
		color:#686868!important;

	}*/
.form-control:focus{
	border-color: <?php echo $button_info['button']['primary']['hover']?>!important;
	box-shadow:none!important;
}
</style>
<?php //echo '<pre>';print_r($button_info['button']['primary']['hover']);die("Er");?>
<?php echo get_header(); ?>
<?php echo get_partial('content_top'); ?>
<div id="page-content">
	<div class="container">
		<div class="row">
			<?php echo get_partial('content_left'); ?>
			<?php
				if (partial_exists('content_left') AND partial_exists('content_right')) {
					$class = "col-sm-5 col-md-6";
				} else if (partial_exists('content_left') OR partial_exists('content_right')) {
					$class = "col-sm-8 col-md-9";
				} else {
					$class = "col-md-12";
				}
			?>

			<div class="<?php echo $class; ?>">
				<div class="row">
					<div class="col-xs-12">
						
					</div>
				</div>

				<?php if ($this->alert->get()) { ?>
					<div id="notification">
						<div class="row">
							<div class="col-md-12">
								<?php echo $this->alert->display(); ?>
							</div>
						</div>
					</div>
				<?php } 

				?>

				<div class="row">
					<div class="col-md-12">
						<form method="POST" accept-charset="utf-8" action="<?php echo $_action; ?>" id="checkout-form" role="form">
							<input type="hidden" name="checkout_step" class="checkout_step" value="two">

							<!-- <div id="checkout" class="content-wrap" style="display: <?php echo ($checkout_step === 'one') ? 'block' : 'none'; ?>"> -->	
							<div id="checkout" class="content-wrap" style="display:block">
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group checkout_pick_up heading-color"  >

											<label><?php echo ($order_type == '1') ? $lang_file_values['text_heading_delivery_details'] : $lang_file_values['text_heading_pick_up_details'] ;?></label>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6" style="display:none">
										<div class="form-group">
											<label for="order-time" class="heading-color" ><?php echo sprintf(lang('label_order_time'), $order_type_text); ?></label>
											<?php if ($order_times) { ?>
												<div class="btn-group" data-toggle="buttons">
													<?php if (!empty($order_times['asap'])) { ?>
														<label class="btn btn-default <?php echo ($order_time_type === 'asap') ? 'btn-primary active' : ''; ?>" data-btn="btn-primary">
															<input type="hidden" name="order_asap_time" value="<?php echo $order_times['asap']; ?>">
															<input type="radio" name="order_time_type" value="asap"  <?php echo ($order_time_type === 'asap') ? 'checked="checked"' : ''; ?>>
															<?php echo lang('text_asap'); ?>
														</label>
													<?php } ?>
													<label class="btn btn-default <?php echo ($order_time_type === 'later') ? 'btn-primary active' : ''; ?>" data-btn="btn-primary"><input type="radio" name="order_time_type" value="later"  <?php echo ($order_time_type === 'later') ? 'checked="checked"' : ''; ?>><?php echo lang('text_later'); ?></label>
												</div>
											<?php } else { ?>
												<br /><?php echo lang('text_location_closed'); ?><br />
											<?php } ?>
											<?php echo form_error('order_time', '<span class="text-danger">', '</span>'); ?>
										</div>
									</div>
									<?php 

									if ($order_times) { ?>
									<div class="clearfix"></div>
										<div id="choose-order-time" class="col-sm-12" style="display: <?php echo ($order_time_type === 'later') ? 'block' : 'none'; ?>;">
											<div class="form-group">
												<label for="choose-order-time" class="heading-color" ><?php echo ($order_type == '1') ? $lang_file_values['text_heading_delivery_time'] : $lang_file_values['text_heading_pick_up_time'];?></label>

												<div class="row order-time-group">
													<div class="col-xs-12 col-sm-5 order-later date-input-addon">
														<div class="select_mate" data-mate-select="active">
															
															<select name="order_date" onchange="" onclick="return false;" id="order-date" class="form-control">
																<?php foreach ($order_times as $date => $times) { ?>
																	<?php if ($date === 'asap' OR empty($times)) continue; ?>

																	<?php if ( ! empty($order_date) AND $date == $order_date) { ?>
																		<option value="<?php echo $date; ?>" selected="selected"><?php echo mdate(lang('text_date_format'), strtotime($date)); ?></option>
																	<?php }else if ( $date != $order_date AND $date == $this->session->userdata('citylocation')['delivery_day']) { ?>
																		<option value="<?php echo $date; ?>" selected="selected"><?php echo mdate(lang('text_date_format'), strtotime($date)); ?></option>
																	<?php } else { 
																		?>
																		<option value="<?php echo $date; ?>"><?php echo mdate(lang('text_date_format'), strtotime($date)); ?></option>
																	<?php } ?>
																<?php } ?>
															</select>
															<p class="selecionado_opcion"  onclick="open_select(this)" ></p>
															<span onclick="open_select(this)" class="icon_select_mate" >
																<svg fill="#000000" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg" class="svgcolor">
																    <path d="M7.41 7.84L12 12.42l4.59-4.58L18 9.25l-6 6-6-6z"/>
																    <path d="M0-.75h24v24H0z" fill="none"/>
																</svg>
															</span>
															<div class="cont_list_select_mate">
															  <ul class="cont_select_int" >  </ul> 
															</div>
															<input type="hidden" name="order_hour" value="<?php echo $order_hour; ?>">
															<input type="hidden" name="order_minute" value="<?php echo $order_minute; ?>">
														</div>
													</div>

													<div class="col-xs-12 col-sm-5 order-later time-input-addon" >
														<?php foreach ($order_times as $date => $times) { ?>
															<?php if ($date === 'asap' OR empty($times)) continue; ?>
															<div id="order-time-<?php echo $date; ?>" class="input-group select_mate" data-mate-select="active" style="display: <?php echo ($date == $order_date) ? 'table' : 'none'; ?>">
																
																<select id="hours-for-<?php echo $date; ?>" data-parent="#order-time-<?php echo $date; ?>" class="form-control hours" onchange="" onclick="return false;">
																	<?php foreach ($times as $hour => $minutes) {
																		if($hour == $order_hour)
																		{
																			foreach ($minutes as $minute) { ?>
																				<?php if ($minute == $order_minute) { ?>
																					<option value="<?php echo $hour.':'.$minute; ?>" selected="selected"><?php echo $hour.':'.$minute; ?></option>
																				<?php } else { ?>
																					<option value="<?php echo $hour.':'.$minute; ?>"><?php echo $hour.':'.$minute; ?></option>
																				<?php }
																			} 

																		}else{



																		 ?>
																				<?php foreach ($minutes as $minute) { ?>
																					<option value="<?php echo $hour.':'.$minute; ?>"><?php echo $hour.':'.$minute; ?></option>
																				<?php } ?>
																	<?php } } ?>
																</select>
																<p class="selecionado_opcion"  onclick="open_select(this)" ></p>
																	<span onclick="open_select(this)" class="icon_select_mate" >
																		<svg fill="#000000" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg" class="svgcolor">
																		    <path d="M7.41 7.84L12 12.42l4.59-4.58L18 9.25l-6 6-6-6z"/>
																		    <path d="M0-.75h24v24H0z" fill="none"/>
																		</svg>
																	</span>
																	<div class="cont_list_select_mate">
																	  <ul class="cont_select_int" onclick="setTimeValue(event);">  </ul> 
																	</div>
																<?php $count = 1; ?>
																
															</div>
														<?php } ?>
													</div>
												</div>
											</div>
										</div>
									<?php } ?>
								</div>
								
								<div class="form-group wrap-top">
									<label for="" class="heading-color" ><?php echo lang('label_comment'); ?></label>
									<textarea name="comment" id="comment" rows="5" class="form-control"><?php echo set_value('comment', $comment); ?></textarea>
									<?php echo form_error('comment', '<span class="text-danger">', '</span>'); ?>
								</div>

								<div class="div_border">&nbsp;</div>
									

								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label class="heading-color" ><?php echo ($order_type === '1') ? $lang_file_values['text_heading_personal_details'] : $lang_file_values['text_heading_personal_details'];?></label>
										</div>
									</div>
								</div>
								
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<label for="first-name"><?php echo lang('label_first_name'); ?></label>
											<input type="text" name="first_name" id="first-name" class="form-control" value="<?php echo set_value('first_name', $first_name); ?>" />
											<?php echo form_error('first_name', '<span class="text-danger">', '</span>'); ?>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label for="last-name"><?php echo lang('label_last_name'); ?></label>
											<input type="text" name="last_name" id="last-name" class="form-control my_fields" value="<?php echo set_value('last_name', $last_name); ?>" />
											<?php echo form_error('last_name', '<span class="text-danger">', '</span>'); ?>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<label for="email"><?php echo lang('label_email'); ?></label>
											<input type="text" name="email" id="email" class="form-control " value="<?php echo set_value('email', $email); ?>" <?php echo $is_logged ? 'disabled' : ''; ?> />
											<?php echo form_error('email', '<span class="text-danger">', '</span>'); ?>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label for="telephone"><?php echo lang('label_telephone'); ?></label>
											<input type="text" name="telephone" id="telephone" class="form-control " value="<?php echo set_value('telephone', $telephone); ?>" />
											<?php echo form_error('telephone', '<span class="text-danger">', '</span>'); ?>
										</div>
									</div>
								</div>
								<!-- <div class="row">
									<div class="div_border">&nbsp;</div>
									<br>
									<div class="col-sm-12">
										<div class="form-group">
											<label><?php echo $lang_file_values['text_heading_payment']?></label>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
										<button class="btn btn-primary"> Check Out with PayPal</button>
										</div>
									</div>
								</div> -->
								<div class="row">
									<div class="div_border">&nbsp;</div>
									<br>
									<div class="col-sm-12 form-group">
										<label for="" class="heading-color"><?php echo lang('label_payment_method'); ?></label><br />
										<div class="list-group">
											
											<?php
											 foreach ($payments as $payment) { ?>
												<?php if (!empty($payment['data'])) { ?>

													<div class=" col-md-6 col-xs-12 col-sm-6 list-group-item divs ">
														<div class="col-md-6 col-xs-6 col-sm-6 ">
															<span >
														 		<?php echo $payment['data']; ?>
														 	</span>

														</div>
														<div class="col-md-6 col-xs-6 col-sm-6 logodiv">
															<?php $src="";
															if($payment['code'] == "cod"){
														
															$src = root_url().'assets/images/checkout/cod.jpg';
														 }elseif($payment['code'] == "paypal_express"){
														 	$src = root_url().'assets/images/checkout/paypal-logo-new.jpg';
														 }elseif($payment['code'] == "stripe"){
														 	// $src= "https://i.kinja-img.com/gawker-media/image/upload/s--XkYSDzxz--/c_scale,fl_progressive,q_80,w_800/hflvykipmc5g22mc3m0m.jpg";
														 	$src = root_url().'assets/images/checkout/credit-card-logos.jpg';
														 }

														 ?>
														 <img src="<?=$src?>" class="img-responsive center-block">
															
														</div>
														
														 <?php if($payment['code'] == "stripe"){

																?>
																<div class="clearfix"></div>
																<div class="col-xs-12 col-lg-12 col-sm-12 col-md-12">
																<div id="stripe-payment" class="wrap-horizontal" style="<?php echo ($payment === 'authorize_net_aim') ? 'display: block;' : 'display: none;'; ?>">
	<?php if (!empty($stripe_token)) { ?>
		<input type="hidden" name="stripe_token" value="<?php echo $stripe_token; ?>" />
	<?php } ?>

	<div class="row">
	    <div class="col-xs-12">
			<div class="stripe-errors"></div>
		</div>
	</div>
	<div class="row">
	    <div class="col-xs-12">
	        <div class="form-group">
	            <label for="input-card-number"><?php echo lang('label_card_number'); ?></label>
	            <div class="input-group">
	                <input type="text" id="input-card-number" class="form-control" name="stripe_cc_number" value="<?php echo set_value('stripe_cc_number', $stripe_cc_number); ?>" placeholder="<?php echo lang('text_cc_number'); ?>" autocomplete="cc-number" size="20" data-stripe="number" required />
	                <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
	            </div>
		        <?php echo form_error('stripe_cc_number', '<span class="text-danger">', '</span>'); ?>
	        </div>
	    </div>
	</div>
	<div class="row">
	    <div class="col-xs-7 col-md-7">
	        <div class="form-group">
	            <label for="input-expiry-month"><?php echo lang('label_card_expiry'); ?></label>
	            <div class="row">
	                <div class="col-xs-6 col-lg-6">
				        <input type="tel" class="form-control" id="input-expiry-month" name="stripe_cc_exp_month" value="<?php echo set_value('stripe_cc_exp_month', $stripe_cc_exp_month); ?>" placeholder="<?php echo lang('text_exp_month'); ?>" autocomplete="off" size="2" data-stripe="exp-month" required data-numeric />
			        </div>
			        <div class="col-xs-6 col-lg-6">
				        <input type="tel" class="form-control" id="input-expiry-year" name="stripe_cc_exp_year" value="<?php echo set_value('stripe_cc_exp_year', $stripe_cc_exp_year); ?>" placeholder="<?php echo lang('text_exp_year'); ?>" autocomplete="off" size="4" data-stripe="exp-year" required data-numeric />
			        </div>
		        </div>
		        <?php echo form_error('stripe_cc_exp_month', '<span class="text-danger">', '</span>'); ?>
		        <?php echo form_error('stripe_cc_exp_year', '<span class="text-danger">', '</span>'); ?>
	        </div>
	    </div>
	    <div class="col-xs-5 col-md-5 pull-right">
	        <div class="form-group">
	            <label for="input-card-cvc"><?php echo lang('label_card_cvc'); ?></label>
	            <input type="tel" class="form-control" name="stripe_cc_cvc" value="<?php echo set_value('stripe_cc_cvc', $stripe_cc_cvc); ?>" placeholder="<?php echo lang('text_cc_cvc'); ?>" autocomplete="off" size="4" data-stripe="cvc" required />
		        <?php echo form_error('stripe_cc_cvc', '<span class="text-danger">', '</span>'); ?>
	        </div>
	    </div>
	</div>
</div>
<script type="text/javascript"><!--
	var forceSSL = "<?php echo $force_ssl; ?>";
	$(document).ready(function() {
		$('input[name="payment"]').on('change', function () {
			if (this.value === 'stripe') {
				if (forceSSL == '1' && location.href.indexOf("https://") == -1) {
					location.href = location.href.replace("http://", "https://");
				}

				$('#stripe-payment').slideDown();
			} else {
				$('#stripe-payment').slideUp();
			}
		});

		$('input[name="payment"]:checked').trigger('change');
	});
--></script>
</div>
																<?php
															}
															?>
														
													</div> 
													<div class="col-md-6 col-xs-12 col-sm-6">
														
													</div>
													
														
													
													
													<div class="clearfix"></div>
												<?php } ?> 
											<?php } ?>
											
										</div>
										<?php echo form_error('payment', '<span class="text-danger">', '</span>'); ?>
									</div>
									
									<?php if ($checkout_terms) {?>
										<div class="col-sm-12 form-group">
											<div class="input-group">
												<span class="input-group-addon button-checkbox">
													<button type="button" class="btn" data-color="info" tabindex="7">&nbsp;&nbsp;<?php echo lang('button_agree_terms'); ?></button>
													<input type="checkbox" name="terms_condition" id="terms-condition" class="hidden" value="1" <?php echo set_checkbox('terms_condition', '1'); ?>>
												</span>
												<span class="form-control"><?php echo sprintf(lang('label_terms'), $checkout_terms); ?></span>
											</div>
											<?php echo form_error('terms_condition', '<span class="text-danger col-xs-12">', '</span>'); ?>
										</div>
										<div class="modal fade" id="terms-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-body">
													</div>
												</div>
											</div>
										</div>
									<?php } ?>
									<div class="col-sm-6 form-group hidden-xs">
										<?php if (!empty($button_order)) { ?>
								            <div class="cart-buttons wrap-none">
								                <div class="center-block ">
								                    <span class="btn-replace text-uppercase"><?php echo $button_order; ?></span>
								                    <?php if (!$is_mobile) { ?>
								                        <a class="btn btn-link btn-block visible-xs" href="<?php echo site_url('cart') ?>"><?php echo lang('button_view_cart'); ?></a>
								                    <?php } ?>
								                </div>
								                <div class="clearfix"></div>
								            </div>
								        <?php } ?>
									</div>
									<div class="col-sm-6 form-group">
										
									</div>

									
								</div>
							</div>


							<!-- <div id="payment" class="content-wrap" style="display: <?php echo ($checkout_step === 'two') ? 'block' : 'none'; ?>"> -->
							<!-- <div id="payment" class="content-wrap" style="display:block">
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<label for=""><?php echo lang('label_customer_name'); ?></label><br /><?php echo $first_name; ?> <?php echo $last_name; ?>
										</div>
										<div class="form-group">
											<label for=""><?php echo lang('label_email'); ?></label><br />
											<?php echo $email; ?>
										</div>
										<div class="form-group">
											<label for=""><?php echo lang('label_telephone'); ?></label><br />
											<?php echo $telephone; ?>
										</div>
										<?php if ($order_type === '1' AND $addresses) { ?>
											<div class="form-group">
												<label for=""><?php echo lang('label_address'); ?></label><br />
												<?php foreach ($addresses as $address) { ?>
													<?php if (!empty($address['address_id']) AND $address_id == $address['address_id']) { ?>
														<address class="text-left"><?php echo $address['address']; ?></address>
													<?php } ?>
												<?php } ?>
											</div>
										<?php } ?>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label for=""><?php echo lang('label_order_type'); ?></label><br /><?php echo ($order_type === '1') ? lang('label_delivery') : lang('label_collection'); ?>
										</div>
										<div class="form-group">
											<label for=""><?php echo sprintf(lang('label_order_time'), $order_type_text); ?></label><br /><?php echo mdate(lang('text_date_format') .' '. config_item('time_format'), strtotime($order_time)); ?>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-sm-6 form-group">
										<label for=""><?php echo lang('label_payment_method'); ?></label><br />
										<div class="list-group">
											
											<?php /*foreach ($payments as $payment) { ?>
												<?php if (!empty($payment['data'])) { ?>
													<div class="list-group-item"><?php echo $payment['data']; ?></div>
												<?php } ?>
											<?php } */ ?>
										</div>
										<?php echo form_error('payment', '<span class="text-danger">', '</span>'); ?>
									</div>

									<?php if ($checkout_terms) {?>
										<div class="col-sm-12 form-group">
											<div class="input-group">
												<span class="input-group-addon button-checkbox">
													<button type="button" class="btn" data-color="info" tabindex="7">&nbsp;&nbsp;<?php echo lang('button_agree_terms'); ?></button>
													<input type="checkbox" name="terms_condition" id="terms-condition" class="hidden" value="1" <?php echo set_checkbox('terms_condition', '1'); ?>>
												</span>
												<span class="form-control"><?php echo sprintf(lang('label_terms'), $checkout_terms); ?></span>
											</div>
											<?php echo form_error('terms_condition', '<span class="text-danger col-xs-12">', '</span>'); ?>
										</div>
										<div class="modal fade" id="terms-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-body">
													</div>
												</div>
											</div>
										</div>
									<?php } ?>

									<div class="col-sm-12 form-group">
										<label for=""><?php echo lang('label_ip'); ?></label>
										<?php echo $ip_address; ?><br /><small><?php echo lang('text_ip_warning'); ?></small>
									</div>
								</div>
							</div> -->
						</form>
					</div>
				</div>
			</div>
			<?php echo get_partial('content_right', 'col-sm-4 col-md-3'); ?>
			<?php echo get_partial('content_bottom'); ?>
		</div>
	</div>
</div>

<script type="text/javascript"><!--
$(document).ready(function() {
//shoow time text only
	$('#choose-order-time').fadeIn();
	$('#address-forms > div').slideUp();

	

	$('input[name="order_time_type"]').on('change', function() { 
		$('#choose-order-time').fadeOut();

		if (this.value === 'later') {
			$('#choose-order-time').fadeIn();
		}
	});

	$('select[name="order_date"]').on('change', function() {
		$('#choose-order-time .time-input-addon .input-group').css("display", "none");

		var timeAddonId = "#order-time-" + this.value;
		if ($(timeAddonId).length) {
			$(timeAddonId).css("display", "table");
			$(timeAddonId + ' select.hours, ' + timeAddonId + ' select.minutes:not(.hide)').trigger("change");
		}
	});

	$('select.hours').on('change', function() {
		
		//var minutesAddonId = ".minutes-for-" + this.value;

		//$('#choose-order-time .time-input-addon .minutes').addClass("hide");
		var array = this.value.split(':');
		$('input[name="order_hour"]').val(array[0]);
		$('input[name="order_minute"]').val(array[1]);

		/*if ($(this).parent().find(minutesAddonId).length) {
			$(minutesAddonId).removeClass("hide");
			$(minutesAddonId).css("display", "table-cell");
			$(minutesAddonId).trigger("change");
		}*/
	});

	$('select.minutes').on('change', function() {
		$('input[name="order_minute"]').val(this.value);
	});

	$('#address-labels input[name="address_id"]').on('change', function() {
		var formToggle = $(this).parent().parent().find('.edit-address');
		formToggle.text('<?php echo lang('text_edit'); ?>');
		$('#address-forms > div').slideUp();
	});

	$('#address-labels .edit-address').on('click', function() {
		var formDiv = $(this).attr('data-form');
		$('#address-forms > div').slideUp();

		if ($(formDiv).is(':visible')) {
			$(this).text('<?php echo lang('text_edit'); ?>');
			$(formDiv).slideUp();
		} else {
			$(this).text('<?php echo lang('text_close'); ?>');
			$(formDiv).slideDown();
		}
	});

	$('.step-one.link a').on('click', function() {
		$(this).removeClass('link');
		$('.step-two').removeClass('active').addClass('disabled');
		$('.step-one').addClass('active');
		$('input[name="checkout_step"]').val('one');
		$('#checkout').fadeIn();
		$('#payment').fadeOut();
		$('#cart-box .btn-order').text('<?php echo lang('button_payment'); ?>');

	});

	/*$(".my_fields11").click(function () {
    $(".my_fields11").toggleClass('btn-primary');
    $(".my_fields11").toggleClass('my_fields');
    });*/

/*
	$('.my_fields11').hover(
   function(){
       $(this).addClass('btn-primary');
       $(this).addClass('my_fields');
   },
   function() {
       $(this).removeClass('btn-primary');
       $(this).removeClass('my_fields');
   })
    .click(
    function() {
      $(this).removeClass('btn-primary');
       $(this).removeClass('my_fields');
 
});*/


});



//--></script>

<?php
if ($order_type == 1) {
	# code...

?>
<script type="text/javascript">
	console.log("loaded");
	$("#testswitch").on("change", function(){
		location.reload();
	});

	 function setTimeValue(event) {
        var target = event.target || event.srcElement;
        alert(event.target.innerHTML);
        var array = event.target.innerHTML.split(':');
		$('input[name="order_hour"]').val(array[0]);
		$('input[name="order_minute"]').val(array[1]);
    }
</script>
<?php
}
?>

<?php echo get_footer(); ?>