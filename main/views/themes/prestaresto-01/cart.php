<?php echo get_header(); ?>
<?php echo get_partial('content_top'); ?>
<!-- wrap-all  add if he wants padding -->
<div class="side-bar" style="padding-top: 20px; ">
    <!-- <div class="cart-buttons wrap-bottom">
        <div class="center-block">
            <a class="btn btn-default btn-block btn-md" href="<?php echo restaurant_url().'/#local-menus'; ?>"><?php echo lang('button_go_back') ?></a>
        </div>
        <div class="clearfix"></div>
    </div> --> 

    <?php echo $this->cart_module->getCart(array(), array(), TRUE); ?>
   <div id="cart-buttons" class="visible-xs">
   		<div class="col-xs-5 text-center">
   			<a class="btn-back-next cart-toggle" href="<?php echo restaurant_url().'/#local-menus'; ?>" style="">   <?php  
	        	echo lang('text_button_back'); 
        	?>
	    	</a> 

   		</div>
   		<div class="col-xs-2 text-center">
   			|
   		</div>
	    <div class="col-xs-5 text-center"> 
	    	<a class="btn-back-next cart-toggle" href="<?php echo base_url().'checkout'?>"><?php  
	        echo lang('text_button_next'); 
        	?></a>
	    </div>
	    
	</div>
</div>
<?php echo get_partial('content_bottom'); ?>
<?php echo get_footer(); ?>