<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

class Cart extends Main_Controller {

    public function index() {
        $this->lang->load('cart');
 
        $this->load->module('cart_module');
        $this->load->model('Languages_model');
        $local_info = $this->session->userdata('local_info');
        
        if (isset($local_info['order_type'])) {
            if($_SESSION['session_order_type']['order_type']==1)
            {
                $data['order_type'] = '1';
            }else{
                $data['order_type'] = '2';
            }    
        } 

		$button_info = $this->Languages_model->button_info(31);
	    $data['button_info'] = unserialize($button_info->data); 
        $data['cart'] = $this->cart_module->getCart(array(), array(), TRUE);

        $this->template->setTitle($this->lang->line('text_heading'));
        $this->template->setStyleTag(extension_url('cart_module/views/stylesheet.css'), 'cart-module-css', '144000');

        $this->template->render('cart', $data);
    }
}