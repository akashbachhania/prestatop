<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_title'] 		                = 'Dashboard';
$lang['text_heading'] 		                = 'Dashboard';
$lang['text_zero'] 		                    = '0';
$lang['text_dash_dash'] 		            = '--';
$lang['text_today'] 		                = 'Today';
$lang['text_delivery'] 		                = 'Delivery';
$lang['text_collection'] 		            = 'Pick-up';
$lang['text_no_activity'] 		            = 'There are no new activities available.';
$lang['text_total_sale'] 		            = 'Total Sales'; 
$lang['text_total_lost_sale'] 		        = 'Total Lost Sales';
$lang['text_total_cash_payment'] 		    = 'Total Cash Payments';
$lang['text_total_customer'] 		        = 'Total Customers';
$lang['text_total_order'] 		            = 'Total Orders';
$lang['text_total_delivery_order'] 		    = 'Total Delivery Orders';
$lang['text_total_collection_order'] 		= 'Total Pick-up Orders';
$lang['text_total_completed_order'] 		= 'Total Orders Completed';
$lang['text_total_reservation'] 		    = 'Total Reservations';
$lang['text_total_reserved_table'] 		    = 'Total Table(s) Reserved';
$lang['text_complete_setup'] 		        = 'Complete Restaurant Setup';
$lang['text_progress'] 		                = 'Complete';
$lang['text_progress_initial'] 		        = 'Initial Setup';
$lang['text_progress_setting'] 		        = 'Your restaurant settings';
$lang['text_progress_menus'] 		        = 'Add Menus';
$lang['text_progress_design'] 		        = 'Design your website background';
$lang['text_progress_email'] 		        = 'Send test email';
$lang['text_progress_count']                = '60%';
$lang['text_progress_summary'] 		        = 'Follow these steps to get your restaurant up &amp; running. ';
$lang['text_initial_progress'] 		        = '<b>Initial Setup</b> - Set your prefered general settings, such as restuarant name and email.';
$lang['text_settings_progress'] 		    = '<b>Your restaurant settings</b> - Let your customers know your restaurant opening times, delivery hours, preparation time and where its located.';
$lang['text_menus_progress'] 		        = '<b>Add Menus</b> - Add your menu using our simple editor. Display multiple menus like delivery &amp; eat-in, includes sizes and meal options';
$lang['text_design_progress'] 		        = '<b>Design your website background</b> - Upload your own or choose a background image for your website from our gallery.';
$lang['text_email_progress'] 		        = '<b>Send test email</b> - Ensure order confirmation emails are sent to customers successfully.';
$lang['text_statistic'] 		            = 'Statistics';
$lang['text_range'] 		                = 'Range';
$lang['text_today'] 		                = 'Today';
$lang['text_week'] 		                    = 'Week';
$lang['text_month'] 		                = 'Month';
$lang['text_year'] 		                    = 'Year';
$lang['text_news'] 		                    = 'TastyIgniter News';
$lang['text_recent_activity'] 		        = 'Recent Activity';
$lang['text_top_customers'] 		        = 'Top Customers';
$lang['text_latest_order'] 		            = '10 Latest Orders';
$lang['text_reports_chart'] 		        = 'Reports Chart';
$lang['text_select_range'] 		            = 'Select date range';
$lang['text_last_version_check'] 		    = 'Your last <b>TastyIgniter core version check</b> was more than a week ago. <a href="%s"><b>Check for Updates</b></a>';

$lang['button_check_updates']       	    = '<i class="fa fa-refresh"></i> Check Updates';

$lang['column_id']       		            = 'ID';
$lang['column_location'] 		            = 'Location';
$lang['column_name'] 		                = 'Customer Name';
$lang['column_status'] 		                = 'Status';
$lang['priority']                       = 'Priority';
$lang['column_type'] 		                = 'Type';
$lang['column_ready_type'] 		            = 'Ready Time';
$lang['column_date_added'] 		            = 'Date Added';
$lang['column_total_orders'] 		        = '# Orders';
$lang['column_total_sale'] 		            = 'Total Sale';


// New added for orders page
$lang['text_list'] 		                = 'Order List';
$lang['text_filter_status'] 		    = 'View all status';
$lang['text_tab_status'] 		        = 'Status';
$lang['text_tab_restaurant'] 		    = 'Restaurant';
$lang['text_tab_delivery_address'] 		= 'Delivery Address';
$lang['text_tab_payment'] 		        = 'Payment';
$lang['text_tab_menu'] 		            = 'Menu Items  &nbsp;<span class="badge">%s</span>';
$lang['text_empty'] 		            = 'There are no orders available.';
$lang['text_no_status_history'] 		= 'There are no status history for this order.';
$lang['text_filter_search'] 		    = 'Search order id, location or customer name.';
$lang['text_filter_location'] 		    = 'View all locations';
$lang['text_filter_status'] 		    = 'View all status';
$lang['text_filter_order_type'] 		= 'View all order types';
$lang['text_filter_payment'] 		    = 'View all payments';  
$lang['text_filter_date'] 		        = 'View all dates';
$lang['text_delivery'] 		            = 'Delivery';
$lang['text_collection'] 		        = 'Pick-up';
$lang['text_order_details'] 		    = 'Order Details';
$lang['text_customer_details'] 		    = 'Customer Details';
$lang['text_status_history'] 		    = 'Status History';
$lang['text_no_match'] 		            = 'No Matches Found';
$lang['text_no_order_comment'] 		    = 'No order comment';
$lang['text_email_sent'] 		        = 'Email SENT';
$lang['text_email_not_sent'] 		    = 'Email not SENT';
$lang['text_transaction_detail'] 		= 'View Transaction Details';
$lang['text_total'] 		            = 'TOTAL';
$lang['text_no_delivery_address'] 		= 'This is a pick-up order, there is no delivery address.';
$lang['text_no_invoice'] 		        = 'Update status to auto-generate invoice';
$lang['text_invoice'] 		            = 'Invoice';
$lang['text_restaurant'] 		        = 'Restaurant';
$lang['text_customer'] 		            = 'Customer';
$lang['text_payment'] 		            = 'Payment';
$lang['text_invoice_no'] 		        = 'Invoice #';
$lang['text_invoice_date'] 		        = 'Invoice Date';
$lang['text_order_date'] 		        = 'Order Date';
$lang['text_deliver_to'] 		        = 'Deliver to';
$lang['text_invoice_thank_you'] 	    = 'Thank you for your order';
$lang['text_collection_order_type'] 	= 'This is a pick-up order';
$lang['text_no_payment'] 	            = 'No payment method selected';
$lang['text_lost_orders'] 		        = 'Lost orders';

$lang['button_create_invoice'] 		    = 'Generate';
$lang['button_view_invoice'] 		    = 'View Invoice';
$lang['button_print_invoice'] 		    = 'Print Invoice';
$lang['button_download_invoice'] 		= 'Download Invoice';

$lang['column_location'] 		        = 'Location';
$lang['column_customer_name'] 		    = 'Customer Name';
$lang['column_customer_telephone'] 		    = 'Telephone';
$lang['column_order_slip'] 		    = 'Order Slip';
$lang['text_see'] 		    = 'See';
$lang['text_print'] 		    = 'Print';

$lang['column_status'] 		            = 'Status';
$lang['column_type'] 		            = 'Type';
$lang['column_payment'] 		        = 'Payment';
$lang['column_total'] 		            = 'Total';
$lang['column_time_date'] 		        = 'Time - Date';
$lang['column_id'] 		                = 'ID';
$lang['column_staff'] 		            = 'Staff';
$lang['column_comment'] 		        = 'Comment';
$lang['column_assignee'] 		        = 'Staff Assignee';
$lang['column_notify'] 		            = 'Customer Notified';
$lang['column_name_option'] 		    = 'Name/Options';
$lang['column_price'] 		            = 'Price';

$lang['label_order_id'] 		        = 'Order #';
$lang['label_customer_name'] 		    = 'Name';
$lang['label_email'] 		            = 'Email';
$lang['label_telephone'] 		        = 'Telephone';
$lang['label_order_type'] 		        = 'Order Type';
$lang['label_order_time'] 		        = 'Delivery/Pick-up Time';
$lang['label_order_date'] 		        = 'Date Added';
$lang['label_order_total'] 		        = 'Order Total';
$lang['label_comment'] 		            = 'Order Comment';
$lang['label_date_modified'] 		    = 'Date Modified';
$lang['label_notify'] 		            = 'Notify Customer';
$lang['label_user_agent'] 		        = 'User Agent';
$lang['label_ip_address'] 		        = 'IP Address';
$lang['label_status'] 		            = 'Order Status';
$lang['label_invoice'] 		            = 'Invoice';
$lang['label_assign_staff'] 		    = 'Assign Staff';
$lang['label_restaurant_name'] 		    = 'Name';
$lang['label_restaurant_address'] 		= 'Address';
$lang['label_payment_method'] 		    = 'Payment Method';

$lang['alert_order_not_completed'] 		= 'The order must reach the completed order status before generating an invoice';
/* End of file dashboard_lang.php */
/* Location: ./admin/language/english/dashboard_lang.php */