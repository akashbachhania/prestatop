<style type="text/css">
.background_img{
    background: rgba(0, 0, 0, 0) url("<?php echo base_url('assets/images/header_img/home_k.jpg');?>") no-repeat scroll 0 0 / cover;
}    
</style>

<div class="background_img hidden-xs">
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12 ">
<h1 class="bg-head">BUNS - PARIS<br><span style="font-size: 30px;">8 rue Gay Lussac 75005<br><span style="font-size: 26px;">+33 1 56 81 67 58<br><span style="font-size: 20px;">Ouvert jusqu'à 22h00</span> </h1>
</div>
 <?php /* if ($location_search !== TRUE AND $rsegment !== 'locations') { ?>
	<div class="clearfix"></div>

	<div class="ppostal center-block">
	<div class="container">
	<div class="row">
	<div class="col-md-5 col-sm-4 col-xs-12">
	<p class="pchek"><?php echo lang('text_no_search_query'); ?> </p>
	</div>
	<div class="col-md-7 col-sm-8 col-xs-12 pmform">
	<form style="align:center;" class="form-inline pcheck">
	  <div class="form-group">
	    <label class="sr-only" for="exampleInput"></label>
	    <input type="text" autocomplete="on" id="searchTextField" class="form-control text-center postcode-control input-xs" name="search_query" placeholder="<?php echo lang('label_search_query'); ?>" value="<?php echo $search_query; ?>">



	    <!-- <input type="text" class="form-control" id="exampleInput" placeholder="Enter Your street Address"> -->
	  </div>
	  
	  <button id="search" type="submit" class="btn btn-default" onclick="searchLocal();"><?php echo lang('button_search_location'); ?></button>
	</form>
	</div>
	</div>
	</div>
	</div>
 <?php } */ ?>
<!--       <input id="searchTextField" type="text" size="50" placeholder="Enter a location" autocomplete="on"> -->


 <?php /*if ($location_search !== TRUE AND $rsegment !== 'locations') { ?>
                <div id="local-info" class="col-md-12" style="display: <?php echo ($local_info) ? 'block' : 'none'; ?>">
                    <div class="panel panel-local display-local">
                        <?php  if ($location_search_mode === 'multi') { ?>
                            <div class="panel-heading">
                                <div class="row local-search bg-warning" style="display: <?php echo (empty($search_query) AND $location_order === '1') ? 'block' : 'none'; ?>">
                                    
                                    <div class="col-xs-12 col-sm-6 center-block">
                                        <div class="postcode-group text-center">
                                            <?php echo lang('text_no_search_query'); ?>
                                            <div class="input-group">
                                                <input type="text" id="search-query" class="form-control text-center postcode-control input-xs" name="search_query" placeholder="<?php echo lang('label_search_query'); ?>" value="<?php echo $search_query; ?>">
                                                <a id="search" class="input-group-addon btn btn-primary" onclick="searchLocal();"><?php echo lang('button_search_location'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                               

                                    
                                </div>
                            </div>
                        <?php } ?>

                    </div>
                </div>
            <?php }*/ ?>






</div>
</div>
	<div class="col-md-12">
	<div id="local-box" <?php echo ($location_search === TRUE) ? 'class="local-box-fluid"' : ''; ?>>
	<div class="container">
		<div class="row">
			<?php if ($location_search === TRUE) { ?>
				<div id="local-search" class="col-md-12 text-center">
					<div class="panel panel-local">
						<div class="panel-body">
							<h2><?php echo lang('text_order_summary'); ?></h2>
							<span class="search-label sr-only"><?php echo lang('label_search_query'); ?></span>
							<div class="col-xs-12 col-sm-6 col-md-5 center-block">
								<?php if ($location_search_mode === 'multi') { ?>
									<form id="location-form" method="POST" action="<?php echo $local_action; ?>" role="form">
										<div class="input-group postcode-group">
											<input type="text" id="search-query" class="form-control text-center postcode-control input-lg" name="search_query" placeholder="<?php echo lang('label_search_query'); ?>" value="<?php echo $search_query; ?>">
											<a id="search" class="input-group-addon btn btn-primary" onclick="searchLocal()"><?php echo lang('text_find'); ?></a>
										</div>
									</form>
								<?php } else { ?>
									<a class="btn btn-block btn-primary" href="<?php echo $single_location_url; ?>"><?php echo lang('text_find'); ?></a>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			<?php } ?>

		</div>
	</div>
<script type="text/javascript"><!--
	$(document).ready(function() {
		$('.review-toggle').on('click', function() {
			$('a[href="#reviews"]').tab('show');
		});
	});

	function toggleLocalSearch() {
		if ($('.panel-local .panel-heading .local-search').is(":visible")) {
			$('.panel-local .panel-heading .local-search').slideUp();
			$('.panel-local .panel-heading .local-change').slideDown();
		} else {
			$('.panel-local .panel-heading .local-search').slideDown();
			$('.panel-local .panel-heading .local-change').slideUp();
		}
	}

	function searchLocal() {
		var search_query = $('input[name=\'search_query\']').val();

		$.ajax({
			url: js_site_url('local_module/local_module/search'),
			type: 'POST',
			data: 'search_query=' + search_query,
			dataType: 'json',
			success: function(json) {
				updateLocalBox(json);
			}
		});
	}

	function updateLocalBox(json) {
		var alert_close = '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
		var local_alert = $('#local-alert .local-alert');
		var alert_message = '';

		if (json['redirect']) {
			window.location.href = json['redirect'];
		}

		if (json['error']) {
			alert_message = '<div class="alert">' + alert_close + json['error'] + '</div>';
		}

		if (json['success']) {
			alert_message = '<div class="alert">' + alert_close + json['success'] + '</div>';
		}

		if ($('#cart-box').is(':visible')) {
			$('#cart-box').load(js_site_url('cart_module/cart_module #cart-box > *'), function (response) {
				if (alert_message != '') {
					local_alert.empty();
					local_alert.append(alert_message);
					$('#local-alert').fadeIn('slow').fadeTo('fast', 0.5).fadeTo('fast', 1.0);
					$('html, body').animate({scrollTop: 0}, 300);
				}
			});
		} else {
			if (alert_message != '') {
				local_alert.empty();
				local_alert.append(alert_message);
				$('#local-alert').fadeIn('slow').fadeTo('fast', 0.5).fadeTo('fast', 1.0);
				$('html, body').animate({scrollTop: 0}, 300);
			}
		}
	}
//--></script>
</div>
</div>

