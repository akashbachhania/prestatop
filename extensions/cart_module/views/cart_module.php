<style type="text/css">
    .pac-container {
        z-index: 10000;
    }

.form-control:focus{
border-color: <?php echo $button_info['button']['primary']['hover']?>!important;
box-shadow:none!important;
}
/*.cart-total .table tr {
    border-bottom: 0px solid !important;

}*/
.cart-items ul li {
    padding: 0px!important;
    margin-top: 0px!important;
}
                            
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 27px;
}
.switchclass{
    cursor: pointer;
}
.switch input {display:none;}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: <?php echo $button_info['button']['primary']['background']?>!important;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 20px;
  width: 20px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

#testswitch:checked + .slider {
  background-color:<?php echo $button_info['button']['primary']['background']?>!important;
}

#testswitch:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

#testswitch:checked + .slider:before {
  -webkit-transform: translateX(25px);
  -ms-transform: translateX(25px);
  transform: translateX(25px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
.switch_label_color_primary{
    color:<?php echo $button_info['button']['primary']['background']?>!important;
}



.cursor-pointer{
    cursor: pointer;
}
</style>
<?php
if($this->uri->segment(1)=='checkout') {
?>
<style type="text/css">
    .cart-items ul li .cart-btn {

margin-top: 8px;

}
</style>
<?php
}
?>    
<?php $menus=$_SESSION['session_order_type']['order_type'];?>
<div class="<?php echo ($is_mobile OR $is_checkout) ? '' : 'hidden-xs'; ?>" 
    <?php echo $fixed_cart; ?>>
    <div id="cart-box" class="module-box">
        <div class="panel panel-default panel-cart <?php echo ($is_checkout) ? 'hidden-xs' : ''; ?>">
            <div class="panel-heading">
                <h3 class="panel-title"><?php echo lang('text_heading'); ?></h3>
            </div>

            <?php if (!empty($cart_alert)) { ?>
            <div class="panel-body">
                <div id="cart-alert" class="cart-alert-wrap">
                    <div class="cart-alert"></div>
                        <?php echo $cart_alert; ?>
                </div>
            </div>
            <?php } ?>
            <?php 
            $text_card_message = lang('text_card_message');
            if (!empty($text_card_message)) { ?>
                <div class="panel-body">
                    <p class="text-center"><?php echo lang('text_card_message'); ?></p>
                </div>
            <?php } ?>
            <?php 
            if ($has_delivery OR $has_collection) { ?>
                <div class="col-md-12 col-xs-12" style="border-bottom: solid 1px grey;padding-bottom: 10px; ">
                    <br>
                    <input class="switch_default" name="switch_default" value="<?php echo ($menus == 2 && $controller=='menus') ? $menus : ''; ?>" type="hidden">
                    <div class="col-md-4 col-xs-4">
                        <span id="delivery_fnt" class="switch_label_color_primary switchclass" data="0">
                            <?php echo lang('text_delivery');?>
                        </span>
                    </div>
                    <div class="col-md-4 col-xs-4">
                        <label class="switch">
                            <input id="testswitch" name="order_type" type="checkbox" data="" <?php echo ($order_type ==2 && $controller=='checkout' || $menus ==2 && $controller=='menus' ) ? 'checked="checked"' : ''; ?>>
                            <div class="slider round"></div>
                        </label>                                   
                    </div>
                    <div class="col-md-4 col-xs-4" style="padding-right: 0 !important">
                        <span id="pickup_fnt" class="switchclass" data="1" >
                            <?php echo lang('text_collection');?>
                        </span>                                    
                    </div>                                
                </div>
                
                <div class="col-md-12 col-xs-12 cart-address" id="default_city_location" style="<?php echo ($order_type ==2 && $controller=='checkout' || $menus ==2 && $controller=='menus'|| $order_type ==2 && $controller=='cart' ) ? '' : 'display:none '; ?>">
                    <p id="default_city_location_value">
                        <b>Pick-Up</b>
                            <?php echo $_SESSION['citylocation']['default_location']?>
                           
                    </p>
                    <span class="pull-right cursor-pointer" id="changer_pick_up" >
                        <b>
                            <span style="font-size: 12px;">changer</span> <span class="fa fa-pencil"></span>
                        </b>
                    </span>
                    <p></p>
                </div> 
                <div class="col-md-12 col-xs-12 cart-address" style="<?php echo ($order_type ==1 && $controller=='checkout' || $menus ==1 && $controller=='menus' || $order_type ==1 && $controller=='cart') ? '' : 'display:none '; ?>" id="show_user_delivery_address_div">
                    <p class="caption">
                        <span>
                            <b>
                                <?php echo lang('text_delivery');?>
                            </b>
                        </span> <span id="show_user_delivery_address"><?php echo $_SESSION['citylocation']['location']?></span></p>
                    <p>
                        <span class="pull-right cursor-pointer" id="changer_delivery">
                            <b><span style="font-size: 12px;">changer</span> <span class="fa fa-pencil"></span></b>
                        </span>
                    </p>
                </div>                       
                   
                <?php } ?>
                <div id="cart-info">
                    <?php if ($cart_items) {?>
                        <div class="cart-items">
                            <ul>
                            <?php foreach ($cart_items as $cart_item) { ?>
                                <li>
                                    <a class="cart-btn remove text-muted small" onClick="removeCart('<?php echo $cart_item['menu_id']; ?>', '<?php echo $cart_item['rowid']; ?>', '0');"><i class="fa fa-trash-o"></i></a>
                                    <a class="name-image" onClick="openMenuOptions('<?php echo $cart_item['menu_id']; ?>', '<?php echo $cart_item['rowid']; ?>');">
                                        <?php if (!empty($cart_item['image'])) { ?>
                                            <img class="image img-responsive img-thumbnail" width="<?php echo $cart_images_w; ?>" height="<?php echo $cart_images_h; ?>" alt="<?php echo $cart_item['name']; ?>" src="<?php echo $cart_item['image']; ?>">
                                        <?php } ?>
                                        <span class="amount pull-right"><?php echo $cart_item['sub_total']; ?></span>

                                        <span class="name">
                                            <span class="quantity"><?php echo $cart_item['qty'].lang('text_times'); ?></span>
                                            <span class="product-name"><?php echo $cart_item['name']; ?></span>
                                        </span>
                                        <?php if (!empty($cart_item['options'])) { ?>
                                            <span class="options text-muted small"><?php echo $cart_item['options']; ?></span>
                                        <?php } ?>
                                    </a>
                                    <p class="comment-amount">
                                        <?php if (!empty($cart_item['comment'])) { ?>
                                            <span class="comment text-muted small">[<?php echo $cart_item['comment']; ?>]</span>
                                        <?php } ?>
                                    </p>
                                </li>
                            <?php } ?>
                            </ul>
                        </div>
                        <div class="cart-coupon">
                            <div class="input-group">
                                <input type="text" name="coupon_code" class="form-control" value="<?php echo isset($coupon['code']) ? $coupon['code'] : ''; ?>" placeholder="<?php echo lang('text_apply_coupon'); ?>" />
                                <span class="input-group-btn"><a class="btn btn-default" onclick="applyCoupon();" title="<?php echo lang('button_apply_coupon'); ?>"><i class="fa fa-check"></i></a></span>
                            </div>
                        </div>
                        <div class="cart-total">
                            <div class="table-responsive">
                                <table width="100%" height="auto" class="table table-none">
                                    <tbody>
                                        <?php foreach ($cart_totals as $name => $total) { ?>
                                            <?php if (!empty($total)) { ?>
                                                <tr>
                                                    <td><span class="text-muted">
                                                        <?php if ($name === 'order_total') { ?>
                                                            <b><?php echo $total['title']; ?>:</b>
                                                        <?php } else if ($name === 'coupon' AND isset($total['code'])) { ?>
                                                            <?php echo $total['title']; ?>:&nbsp;&nbsp;
                                                            <a class="remove clickable" onclick="clearCoupon('<?php echo $total['code']; ?>');"><span class="fa fa-times"></span></a>
                                                        <?php } else { ?>
                                                            <?php echo $total['title']; ?>:
                                                        <?php } ?>
                                                    </span></td>
                                                    <td class="text-right">
                                                        <?php if ($name === 'coupon') { ?>
                                                            -<?php echo $total['amount']; ?>
                                                        <?php } else if ($name === 'order_total') { ?>
                                                            <b><span class="order-total"><?php echo $total['amount']; ?></span></b>
                                                        <?php } else { ?>
                                                            <?php echo $total['amount']; ?>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="panel-body"><?php echo lang('text_no_cart_items'); ?></div>
                    <?php } ?>
                </div>
            </div>
        </div>

        <?php
                $last = $this->uri->total_segments();
                $record_num = $this->uri->segment($last);
            if($this->uri->segment($last) == "checkout")
            {
                ?>
            <div class="cart-buttons wrap-none hidden-xs">
                <div class="center-block ">
                    <span class="btn-replace text-uppercase">
                        <a class="btn btn-order btn-back-menu btn-block btn-lg" onclick="history.back()">
                            Back To Menu
                        </a>
                    </span>
                </div>
                <div class="clearfix"></div>
            </div>
        <?php
            }else{
         if (!empty($button_order)) { ?>
        
            <div class="cart-buttons wrap-none hidden-xs">
                <div class="center-block ">
                    <span class="btn-replace text-uppercase">
                        <?php echo $button_order; ?></span>
                    <?php if (!$is_mobile) { ?>
                        <a class="btn btn-link btn-block visible-xs" href="<?php echo site_url('cart') ?>"><?php echo lang('button_view_cart'); ?></a>
                    <?php } ?>
                </div>
                <div class="clearfix"></div>
            </div>
        <?php } 
        }
        ?>
    </div>
</div>
<?php
if ($this->config->item('maps_api_key')) {
        $map_key = '&key=' . $this->config->item('maps_api_key');
    } else {
        $map_key = '';
    }
if($this->uri->segment(1)=='checkout' ||$this->uri->segment(1)=='cart') {
?>

<script src="https://maps.googleapis.com/maps/api/js?<?php echo $map_key;?>&libraries=places&ver=2.1.1"></script>
<?php } ?>
<?php if($this->uri->segment(1)=='checkout'){ ?>
<div id="cart-buttons" class="visible-xs">
   <div class="cart-buttons wrap-none">
        <div class="center-block text-center">
            <span class="btn-replace text-uppercase">
                <?php echo $button_order; ?></span>
            
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<?php } ?>
<div id="cart-buttons" class="<?php echo (!$is_mobile AND !$is_checkout) ? 'visible-xs' : 'hide'; ?> text-center">
    <a class="btn-back-next cart-toggle" href="<?php echo site_url('cart') ?>" style="text-overflow:ellipsis; overflow:hidden;">  

        <?php  
         echo lang('text_button_heading'); ?> 
        <span class="order-total"><?php echo (!empty($order_total)) ? '&nbsp;&nbsp;-&nbsp;&nbsp;'.$order_total : ''; ?></span>
    </a>
</div>
<?php if (!$is_mobile) { ?>
<div class="cart-alert-wrap cart-alert-affix visible-xs-block"><div class="cart-alert"></div><?php if (!empty($cart_alert)) { echo $cart_alert; } ?></div>
<?php } ?>
<script src="http://rawgit.com/Logicify/jquery-locationpicker-plugin/master/dist/locationpicker.jquery.js">
</script>


<script type="text/javascript"><!--
    var delivery_address_location=null;
    var alert_close = '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';

    var cartHeight = pageHeight-(75/100*pageHeight);

    $(document).on('ready', function() {
        $('.cart-alert-wrap .alert').fadeTo('slow', 0.1).fadeTo('slow', 1.0).delay(5000).slideUp('slow');
        $('#cart-info .cart-items').css({"height" : "auto", "max-height" : cartHeight, "overflow" : "auto","overflow-x" : "hidden", "padding-right" : "5px"});

        $(window).bind("load resize", function() {
            var sideBarWidth = $('#content-right .side-bar').width();
            $('#cart-box-affix').css('width', sideBarWidth);
        });
    });


    $(document).on('change', 'input[name="order_type"]', function() { 
        if (typeof this.value !== 'undefined') {
            var order_type = this.value;
            $.ajax({
                url: js_site_url('cart_module/cart_module/order_type'),
                type: 'post',
                data: 'order_type=' + order_type,
                dataType: 'json',
                success: function (json) {
                    if (json['redirect'] && json['order_type'] == order_type) {
                       //window.location.href = json['redirect'];
                    }
                }
            });
        }
    });

    function addToCart(menu_id, quantity) {
        console.log(quantity);
        //alert(menu_id); 
        if ($('#menu-options' + menu_id).length) {
            var data = $('#menu-options' + menu_id + ' input:checked, #menu-options' + menu_id + ' input[type="hidden"], #menu-options' + menu_id + ' select, #menu-options' + menu_id + ' textarea, #menu-options' + menu_id + '  input[type="text"]');
        } else {
            var data = 'menu_id=' + menu_id + '&quantity=' + quantity;
        }

        $('#menu'+menu_id+ ' .add_cart').removeClass('failed');
        $('#menu'+menu_id+ ' .add_cart').removeClass('added');
        if (!$('#menu'+menu_id+ ' .add_cart').hasClass('loading')) {
            $('#menu'+menu_id+ ' .add_cart').addClass('loading');
        }

        $.ajax({
            url: js_site_url('cart_module/cart_module/add'),
            type: 'post',
            data: data,
            dataType: 'json',
            success: function(json) {
                $('#menu'+menu_id+ ' .add_cart').removeClass('loading');
                $('#menu'+menu_id+ ' .add_cart').removeClass('failed');
                $('#menu'+menu_id+ ' .add_cart').removeClass('added');

                if (json['option_error']) {
                    $('#cart-options-alert .alert').remove();

                    $('#cart-options-alert').append('<div class="alert" style="display: none;">' + alert_close + json['option_error'] + '</div>');
                    $('#cart-options-alert .alert').fadeIn('slow');

                    $('#menu' + menu_id + ' .add_cart').addClass('failed');
                } else {
                    $('#optionsModal').modal('hide');

                    if (json['error']) {
                        //$('#menu' + menu_id + ' .add_cart').addClass('failed');
                    }

                    if (json['success']) {
                        //$('#menu' + menu_id + ' .add_cart').addClass('added');
                       // $('#menu' + menu_id + ' .add_cart').remove();
                        $('#menu'+menu_id+' .menu__item__wrapper .menu-content .menu__item__name .shopping').html('<i class="fa fa-shopping-cart"></i>');
                    }

                    updateCartBox(json);
                }
            }
        });
    }

    function openMenuOptions(menu_id, row_id) {
        var checkmenuitem = "<?php echo count($cart_items);?>";
        //alert($('.switch_default').val());
        //alert(checkmenuitem);
        //alert(delivery_address_location);


        if(checkmenuitem==0 && $('.switch_default').val()==''){
            console.log(delivery_address_location);
             opentimelocation(1);
        }
        else
        {
            if (menu_id) {
                var row_id = (row_id) ? row_id : '';

                $.ajax({
                    url: js_site_url('cart_module/cart_module/options?menu_id=' + menu_id + '&row_id=' + row_id),
                    dataType: 'html',
                    success: function(html) {
                        if(html=='update')
                        {
                            addToCart(menu_id,1);
                        }  else{
                            $('#optionsModal').remove();
                            $('body').append('<div id="optionsModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>');
                            $('#optionsModal').html(html);

                            $('#optionsModal').modal();
                            $('#optionsModal').on('hidden.bs.modal', function(e) {
                                $('#optionsModal').remove();
                            });
                        }  

                       

                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        // alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }
    }
    function removeCart(menu_id, row_id, quantity) {
        $.ajax({
            url: js_site_url('cart_module/cart_module/remove'),
            type: 'post',
            data: 'menu_id' + menu_id + '&row_id=' + row_id + '&quantity=' + quantity,
            dataType: 'json',
            success: function(json) {
            $('#menu'+menu_id+' .menu__item__wrapper .menu-content .menu__item__name .shopping .fa-shopping-cart').remove();
            //$('#menu'+menu_id+' .menu__item__wrapper  .menu__item__price .add_plus').html('<a class="menu__item__price-button btn-cart add_cart"><i class="menu__item__price-button-icon icon-plus"></i></a>');
                updateCartBox(json)
            }
        });
    }

    function applyCoupon() {
        var coupon_code = $('#cart-box input[name="coupon_code"]').val();
        $.ajax({
            url: js_site_url('cart_module/cart_module/coupon'),
            type: 'post',
            data: 'action=add&code=' + coupon_code,
            dataType: 'json',
            success: function(json) {
                updateCartBox(json)
            }
        });
    }

    function clearCoupon(coupon_code) {
        $('input[name=\'coupon\']').attr('value', '');

        $.ajax({
            url: js_site_url('cart_module/cart_module/coupon'),
            type: 'post',
            data: 'action=remove&code=' + coupon_code,
            dataType: 'json',
            success: function(json) {
                updateCartBox(json)
            }
        });
    }

    function updateCartBox(json) {
        var alert_message = '';

        if (json['redirect']) {
            window.location.href = json['redirect'];
        }

        if (json['error']) {
            alert_message = '<div class="alert">' + alert_close + json['error'] + '</div>';
            updateCartAlert(alert_message);
        } else {
            /*if (json['success']) {
                alert_message = '<div class="alert">' + alert_close + json['success'] + '</div>';
            }*/

            var flag= $('#testswitch').is(':checked');
            var delivery_address_location= $('#show_user_delivery_address').text();
            var pageURL = $(location).attr("href");
            pageURL=pageURL.substr(pageURL.lastIndexOf('/') + 1);


            $('#cart-box').load(js_site_url('cart_module/cart_module #cart-box > *'), function(response) {
                
                updateCartAlert(alert_message);
                if(flag){

                    $('#delivery_fnt').removeClass('switch_label_color_primary');
                    $('#pickup_fnt').addClass('switch_label_color_primary');
                    $.ajax({
                    url: js_site_url('cart_module/cart_module/default_city_location'),
                    type: 'post',
                        success: function(html) {
                            $("#show_user_delivery_address_div").hide();
                            $("#default_city_location").show();
                            // $("#default_city_location_value").append(' ' +html);

                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            // alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }

                    });
                    $('#testswitch').prop('checked', true);
                    $('.switch_default').val(2);
                    if(pageURL=='checkout')
                    {
                        $('#checkout-delivery').hide();
                        $('#choose-order-time .form-group label').text('Pick-up Time :');
                        $('.checkout_pick_up label').text('Pick-up Details :');
                    }
                }else{
                    $('#pickup_fnt').removeClass('switch_label_color_primary');
                    $('#delivery_fnt').addClass('switch_label_color_primary');
                    var cartHeight = pageHeight-(73/100*pageHeight);
                    $('.cart-alert-wrap .alert').fadeTo('slow', 0.1).fadeTo('slow', 1.0).delay(5000).slideUp('slow');
                    $('#cart-info .cart-items').css({"height" : "auto", "max-height" : cartHeight, "overflow" : "auto", "padding-right" : "5px","overflow-x" : "hidden"});
                    $("#show_user_delivery_address_div").show();
                    $("#show_user_delivery_address").text(delivery_address_location);
                    $('#testswitch').prop('checked', false);
                    $('.switch_default').val(1);

                    if(pageURL=='checkout')
                    {
                        $('#checkout-delivery').show();
                        $('#choose-order-time .form-group label').text('Delivery Time :');
                        $('.checkout_pick_up label').text('Delivery Details :');
                        $('.checkout-delivery label').text('Delivery Address :');

                    }
                }

            });

        }
    }

    function updateCartAlert(alert_message) {
        if (alert_message != '') {
            $('.cart-alert-wrap .alert, .cart-alert-wrap .cart-alert').empty();
            $('.cart-alert-wrap .cart-alert').append(alert_message);
            $('.cart-alert-wrap .alert').slideDown('slow').fadeTo('slow', 0.1).fadeTo('slow', 1.0).delay(5000).slideUp('slow');
        }

        if ($('#cart-info .order-total').length > 0) {
            $('#cart-box-affix .navbar-toggle .order-total').html(" - " + $('#cart-info .order-total').html());
            $('.cart-toggle .order-total').html(" - " + $('#cart-info .order-total').html());
        }

        $('#cart-info .cart-items').css({"height" : "auto", "max-height" : cartHeight, "overflow" : "auto", "padding-right" : "5px","overflow-x" : "hidden"});
    }

    function opentimelocation(order_type) {

            $.ajax({
            url: js_site_url('cart_module/cart_module/citylocation'),
            type: 'post',
            data:'order_type=' + order_type,
                success: function(html) {
                    $('#timelocation').remove();
                    $('body').append('<div id="timelocation" class="modal fade timelocation_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>');
                    $('#timelocation').html(html);
                    $('#timelocation').modal({backdrop: 'static'});
                    $('#timelocation').on('hidden.bs.modal', function(e) {
                        $('#timelocation').remove();
                        // location.reload();
                    });

                },
                error: function(xhr, ajaxOptions, thrownError) {
                    // alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }

            });
    }


$(document).on('click','.savelocation',function() { 
var pageURL = $(location).attr("href");
pageURL=pageURL.substr(pageURL.lastIndexOf('/') + 1)

if($('.street1').val()=='')
{

    $('.addressvalidation').show();

    $('.addressvalidation span').html('Street address is not present').show();

}
else if($('.map_address').val()=='') {
    $('.addressvalidation').show();
    $('.addressvalidation span').html('Address is not present').show();
} else {
 
$('.spinner').show();
var postdata  ='address_1=' +  $('.street1').val() + '&address_2=' + $('.route').val() + '&city=' + $('.city').val() + '&state=' + $('.state').val() + '&postcode=' + $('.zip').val()+ '&country=' + $('.country').val() + '&location=' + $('.location').val() +'&order_type=' + $('#order_type').val() + '&location_lat=' + $('#us3-lat').val() + '&location_lng=' + $('#us3-lon').val()+ '&location=' + $('.location').val();
$('.addressvalidation').hide();
    $.ajax({
        url: js_site_url('cart_module/cart_module/setcitylocation'),
        type: 'post',
        data:postdata,
        success: function(html) { 
            $('.spinner').hide();
            
            if(html=='success')
            {   
                delivery_address_location=$('.map_address').val();
                
                $("#show_user_delivery_address").text(delivery_address_location);
                $('.timelocation1').modal('hide');
                $('#timelocation').modal('hide');

                if(pageURL=='checkout')
                {
                    location.reload();
                }

                $('#cart-box').load(js_site_url('cart_module/cart_module #cart-box > *'), function(response) {
                    var cartHeight = pageHeight-(73/100*pageHeight);
                    
                    $('.cart-alert-wrap .alert').fadeTo('slow', 0.1).fadeTo('slow', 1.0).delay(5000).slideUp('slow');
                    $('#cart-info .cart-items').css({"height" : "auto", "max-height" : cartHeight, "overflow" : "auto", "padding-right" : "5px","overflow-x" : "hidden"});
                    $("#show_user_delivery_address_div").show();
                    $("#show_user_delivery_address").text(delivery_address_location);
                    $('.switch_default').val(1);


                });
            }
            if(html=='fail')
            {
                $('.addressvalidation').show();
                $('.addressvalidation span').html('Address is not present').show();
            }   
        },
        error: function(xhr, ajaxOptions, thrownError) {
            // alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }

    });
}
});
$(window).ready(function() {
    // Do something\
    if($(window).width() <= 767)
    {
        $("#cart-buttons #btn-new-style").removeClass("btn btn-order btn-primary btn-block btn-lg");
        $("#cart-buttons #btn-new-style").addClass("btn-back-next btn-lg");
    }
});
$(window).resize(function() {
    // Do something\
    if($(window).width() <= 767)
    {
        $("#cart-buttons #btn-new-style").removeClass("btn btn-order btn-primary btn-block btn-lg");
        $("#cart-buttons #btn-new-style").addClass("btn-back-next btn-lg");
    }
});
$(document).on('click','#testswitch,.switchclass',function(){
    if($(this).attr('data')!=''){
        if($(this).attr('data')==1){
            $('#testswitch').prop('checked',true);
        }
        else{
            $('#testswitch').prop('checked',false);
        }
    }


    var checked_order_type=0;
    if($('#testswitch').is(':checked') || $(this).attr('data')==1){

        $('#delivery_fnt').removeClass('switch_label_color_primary');
        $('#pickup_fnt').addClass('switch_label_color_primary');
        $.ajax({
            url: js_site_url('cart_module/cart_module/default_city_location'),
            type: 'post',
                success: function(html) {
                    $("#show_user_delivery_address_div").hide();
                    $("#default_city_location").show();
                    // $("#default_city_location_value").append(' ' +html);

                },
                error: function(xhr, ajaxOptions, thrownError) {
                    // alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }

            });
        checked_order_type = 2;       
        $('.switch_default').val(checked_order_type);
        

    }
    else{
        $('#pickup_fnt').removeClass('switch_label_color_primary');
        $('#delivery_fnt').addClass('switch_label_color_primary');
        $("#default_city_location").hide();
        opentimelocation(1);
        checked_order_type = 1;       
    }
        var order_type = checked_order_type;
        $.ajax({
            url: js_site_url('cart_module/cart_module/order_type'),
            type: 'post',
            data: 'order_type=' + order_type,
            dataType: 'json',
            success: function (json) {
                $('.switch_default').val(json.order_type);
                // $(".btn-replace").html(json.button_order);
            }
        });
        if(checked_order_type=2)
        {
            coupon_code='';
            $.ajax({
                url: js_site_url('cart_module/cart_module/coupon'),
                type: 'post',
                data: 'action=remove&code=' + coupon_code,
                dataType: 'json',
                success: function(json) {
                    updateCartBox(json)
                }
            });
        }    




});
$(document).on("click",'#changer_pick_up',function(){
     opentimelocation(1);       
});
$(document).on("click",'#changer_delivery',function(){
     opentimelocation(1);       
});
$(document).on("click",'.savepickup',function(){
    $('.timelocation1').modal('hide');
    $('#timelocation').modal('hide');
    $('#testswitch').prop('checked',true);
    $.ajax({
            url: js_site_url('cart_module/cart_module/default_city_location'),
            type: 'post',
                success: function(html) {
                    $("#show_user_delivery_address_div").hide();
                    $("#default_city_location").show();
                    // $("#default_city_location_value").append(' ' +html);

                },
                error: function(xhr, ajaxOptions, thrownError) {
                    // alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }

            });
    $('#pickup_fnt').addClass('switch_label_color_primary');
    $('#delivery_fnt').removeClass('switch_label_color_primary');
});
$(document).on('click','.timelocation_popup .modal-dialog .modal-header .modal-close-button',function(){
    $('.switch_default').val('');
    
});
    //--></script>
