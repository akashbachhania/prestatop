
<style type="text/css">
    .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control{
        background-color: #fff;
    }

    .modal.in .modal-dialog {
    transform: translate(0px, 0px);
}
.modal-close-button {
 /*   background-image: url("<?php echo $img_path?>");
    background-position: 0 -151px;
*/      border-radius: 50%;
    cursor: pointer;
    font-size: 16px;
    height: 33px;
    position: absolute;
    width: 35px;
    right: 15px;
    top: -34px;

}

.modal.fade .modal-dialog {
    transform: translate(0px, 0px);
    transition: transform 0.3s ease-out 0s;
}
.topping-modal .modal-dialog {
    margin-top: 70px;
    width: 600px;
}
.topping-modal .modal-dialog {
    margin-bottom: 0;
    margin-top: 0;
    width: 100%;
}
.toppings-product__name  b {
    line-height: initial!important;
    padding: initial!important;
    font-weight:700px;
}
.toppings-product__name b {
    font-family: "MuseoSans-700",Arial,sans-serif!important;
    font-weight:700px!important;
    font-size: 1.4rem!important;
    line-height: 1.5!important;
    padding: 15px 20px 0 15px!important;
}
.toppings-product__price b {
    line-height: initial!important;
    padding: initial!important;
}
.toppings-product__price b {
    font-family: "MuseoSans-700",Arial,sans-serif!important;
    font-weight:700px!important;
    font-size: 1.4rem!important;
    line-height: 1.41!important;
    padding: 0 15px!important;
}
.toppings-product__description {
    font-size: 1.4rem!important;
    line-height: 1.43!important;
    padding: 10px 0 15px!important;
}
.toppings-product__description {
    color: #666!important;
    font-size: 1.2rem!important;
    line-height: 1.41!important;
    padding: 0 15px 10px!important;
}
    modal-footer::before, .modal-footer::after {
    content: " ";
    display: table;
}
.modal-footer::after {
    clear: both;
}
.modal-footer::before, .modal-footer::after {
    content: " ";
    display: table;
}
.topping-modal .modal-footer {
    padding: 0 20px 20px;
}
.topping-modal .modal-footer {
    border-top: medium none;
    padding: 15px;
}
.modal-footer {
    position: relative;
}
.modal-footer {
    border-top: 1px solid #e5e5e5;
    padding: 15px;
    text-align: right;
}
a.button {
    padding: 0;
}
.toppings-add__to__cart {
    float: left;
    min-height: 1px;
    padding-left: 10px;
    padding-right: 10px;
    position: relative;
    width: 50%;
}
.button, .button-secondary, .button-secondary--selected.charity__select-button:hover, .ios-smart-banner__view-app-link, .android-smart-banner__view-app-link, .button-secondary--no-hover {
/*    background-color: #d70f64;*/
/*    border: 0 none;
    border-radius: 4px;
    color: #fff;
    display: block;*/
    font-family: "MuseoSans-500",Arial,sans-serif;
    font-size: 1.4rem;
    height: 44px;
    padding: 0 20px;
    position: relative;
    text-align: center;
    text-transform: uppercase;
    transition: all 0.3s cubic-bezier(0.25, 0.46, 0.45, 0.94) 0s;
}
.pull-right, .toppings-add__to__cart {
    float: right !important;
}
.button__text {
    display: block;
    left: 0;
    padding: 0 20px;
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    width: 100%;
}

</style>
<style type="text/css">
    .toppings {
    border-top: 1px solid #bcbcbc;
}
.topping__header {
    font-size: 1.4rem;
    line-height: 1.9;
}
.topping__header {
    border-bottom: 1px solid #bcbcbc;
    cursor: pointer;
    font-family: "MuseoSans-700",Arial,sans-serif !important;
    font-size: 1.2rem;
    line-height: 2.13;
    margin: 0;
    overflow: auto;
    padding: 10px 0;
    text-transform: uppercase;
    transition: background-color 0.1s linear 0s;
}
.topping__header__name {
    padding-left: 0;
}
.topping__header__name {
    float: left;
    min-height: 1px;
    padding-left: 10px;
    padding-right: 10px;
    position: relative;
    width: 50%;
}
.topping-option:first-child {
    margin-top: 15px;
}
.topping-option {
    padding: 10px 0;
}
.topping-option {
    color: #666;
    cursor: pointer;
    font-size: 1.4rem;
    line-height: 2;
    overflow: auto;
    padding: 10px 15px;
    transition: background-color 0.1s linear 0s;
}
.topping-option__button {
    background-image: url("<?php echo $img_path;?>");
    background-position: 0 -32px;
    display: inline-block;
    height: 18px;
    position: relative;
    top: 1px;
    vertical-align: text-top;
    width: 18px;
}
.selected .topping-option__button {
    background-image: url("<?php echo $img_path;?>");;
    background-position: 0 -86px;
    height: 18px;
    width: 18px;
}
.topping-option__button_checkbox {
    background-image: url("<?php echo $img_path;?>");
    background-position: 0 -104px;
    display: inline-block;
    height: 18px;
    position: relative;
    top: 1px;
    vertical-align: text-top;
    width: 18px;
}
.selected .topping-option__button_checkbox {
    background-image: url("<?php echo $img_path;?>");;
    background-position: 0 -14px;
    height: 18px;
    width: 18px;
}
.topping-option {
    color: #666;
    cursor: pointer;
    font-size: 1.4rem;
    line-height: 2;
}
 .topping__header__info {
    float: left;
    min-height: 1px;
    padding-left: 10px;
    padding-right: 0;
    position: relative;
    width: 50%;
}

.icon-up-open-big::before {
    content: "B";
}
[class^="icon-"]::before, [class*=" icon-"]::before {
/*    font-family: "icomoon" !important;*/
    font-style: normal;
    font-variant: normal;
    font-weight: normal;
    line-height: 1em;
    text-transform: none;
    font-weight: bolder;
}
.topping__header__arrow {
    padding-right: 0;
}
.topping__header__arrow {
    color: #d70f64;
    float: right;
    padding: 6px;
}
[class^="icon-"], [class*=" icon-"] {
    line-height: 1em;
}
.topping__header__comment {
    float: none;
    width: auto;
}
.topping__header__comment {
    color: #292929;
    float: right;
    font-family: "MuseoSans-300",Arial,sans-serif;
    overflow: auto;
    padding-right: 0;
    text-transform: none;
    width: 80%;
}
.text-right, .home__stats__comment-minutes, .topping__header__comment {
    text-align: right;
}
.selection-required .topping__comment__help-text {
    color: #d70f64;
}
.topping__comment__help-text {
    color: #bcbcbc;
}

.icon-up-open-big::before {
    content: "B";
}
/*[class^="icon-"]::before, [class*=" icon-"]::before {
    font-family: "icomoon" !important;
    font-style: normal;
    font-variant: normal;
    font-weight: normal;
    line-height: 1em;
    text-transform: none;
}*/
.topping__header__arrow {
    padding-right: 0;
}
.topping__header__arrow {
    color: #d70f64;
    float: right;
    padding: 6px;
}
[class^="icon-"], [class*=" icon-"] {
    line-height: 1em;
}
.topping__options {
    border-bottom: 1px solid #bcbcbc;
    overflow: auto;
}

.topping-option:first-child {
    margin-top: 15px;
}
.topping-option {
    padding: 10px 0;
}
.topping-option {
    color: #666;
    cursor: pointer;
    font-size: 1.4rem;
    line-height: 2;
    overflow: auto;
    padding: 10px 15px;
    transition: background-color 0.1s linear 0s;
}
.topping-option__name {
    width: 95%;
}
.topping-option__name {
    float: left;
    padding-left: 0;
    width: 87%;
}
.modal-close-button {
    right: 15px;
    top: -16px;
}
.close {
    margin-right: 23px;
    margin-top: 5px;
    width: 12px;
}
    .map-responsive{
    overflow:hidden;
    position:relative;
    height:0;
}
.spinner {
    position: fixed;
    top: 50%;
    left: 50%;
    margin-left: -50px; /* half width of the spinner gif */
    text-align:center;
    z-index:1234;
    overflow: auto;
    width: 100px; /* width of the spinner gif */
}
</style>
<div class="modal-dialog modal-md" style=";">
    <div class="modal-content">
        <div class="modal-header">
            <?php if($order_type==2) { ?>
            <h4><?php  echo $this->lang->line('top_pick_up_header');?></h4>
            <?php } else { ?>
            <h4><?php  echo $this->lang->line('top_deliver_header');?></h4>
            <?php  } ?>



                    <div class="modal-close-button btn btn-primary" data-dismiss="modal">X</div>
        </div>
        <div class="modal-body timelocation1" id="timelocation">
            <div class="row">

                <div class="col-md-12">
            <strrong>
            <?php if($order_type==2) { ?>
            <h4><?php  echo $this->lang->line('visit_us_for_pick_up');?></h4>
            <?php } else { ?>
            <h4><?php  echo $this->lang->line('visit_us_for_deliver');?></h4>
            <?php  } ?>


            </strrong>
                <div class="col-md-12 addressvalidation" style="display:none">
                    <div role="alert" class="alert alert-danger alert-dismissable alert-collapsible">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                     <span></span>       
                    </div>
                </div>
                <div class="form-group ">
                    <?php if($order_type==2) { ?>
                    <input type="text" readonly placeholder="Enter your address" name="location" class="form-control input-lg " value="<?php echo $default_location;?>" >
                    <?php } else { ?>
                    <input type="text" placeholder="Enter your address" name="location" id="us3-address" autocomplete="on" class="form-control input-lg location map_address " >

                    <?php  } ?>

                </div>
                <div class="form-group">
                    <input type="hidden" class="form-control" id="us3-radius" />
                    <input type="hidden" class="form-control" id="us3-lat"  name="lat"/>
                    <input type="hidden" class="form-control"  id="us3-lon" name="long" />
                    <input type="hidden" class="form-control"  id="order_type" name="order" value="<?php echo $order_type;?>" />

                </div>

                <div id="spinner" class="spinner" style="display:none;">
                    <img id="img-spinner" src="<?php echo base_url()?>/assets/images/loadernew.gif"" alt="Loading"/>
                </div>

                <div id="us3" class="map-responsive" style="<?php if($order_type==2) { echo 'width: 98%; height: 340px';}?>" ></div>
                <div class="clearfix">&nbsp;</div>



                 <table id="address" style="display:none;">
                  <tr>
                    <td class="label">Street address</td>
                    <td class="slimField"><input class="field street1" 
                          disabled="true" value="<?php if($order_type==2) echo $default_location;?>"></input></td>
                    <td class="wideField" colspan="2"><input class="field route" id="route"
                          disabled="true"></input></td>
                  </tr>
                  <tr>
                    <td class="label">City</td>
                    <td class="wideField" colspan="3"><input class="field city" id="#us3-city"
                          disabled="true"></input></td>
                  </tr>
                  <tr>
                    <td class="label">State</td>
                    <td class="slimField"><input class="field state"
                          id="#us3-state" disabled="true"></input></td>
                    <td class="label">Zip code</td>
                    <td class="wideField"><input class="field zip" id="#us3-zip"
                          disabled="true"></input></td>
                  </tr>
                  <tr>
                    <td class="label">Country</td>
                    <td class="wideField" colspan="3"><input class="field country"
                          id="#us3-country" disabled="true"></input></td>
                  </tr>
                </table>
                   <!-- <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                            <label class="label-pre" for="delivery_day">Day</label>
                                <select class="form-control input-lg delivery_day day_select">
                                <option value="">Select</option>
                                <?php /*foreach ($opening_time as $key => $value) { ?>
                                <option value="<?php echo date('d-m-Y')?>"><?php echo $value['day'];?></option>
                                <?php }*/ ?>
                                 </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                            <label class="label-pre" for="delivery_time">Time</label>
                                <select class="form-control input-lg delivery_time" >
                                </select>
                                <input name="order_type" type="hidden" id='popup_order_type' value="<?php //echo $order_type?>">
                            </div>
                        </div>
                    </div> -->
                                                    
                    
                <div class="modal-footer">

                    <a class="toppings-add__to__cart button btn-block savelocation  btn btn-primary" onclick="" title="">
                         <span class="button__text">
                         <?php if($order_type==1) { ?>
                           Save
            
                        <?php } else { ?>
                            Ok
                        <?php  } ?>   
                        </span>    
                    </a>
                </div>
        
                    
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript"><!--
$(document).ready(function() {
$('#timelocation').hide();
});
$(document).on('click','.modal-close-button',function() { 
    $('#timelocation').modal('hide');
});

$('#us3').locationpicker({
    location: {
        latitude    : <?php echo ($order_type==2) ? $location_lat: 0;?>,
        longitude   : <?php echo ($order_type==2) ? $location_lng : 0;?>
    },
    radius: <?php echo $location_radius;?>,
    onchanged: function (currentLocation, radius, isMarkerDropped) {
        var addressComponents = $(this).locationpicker('map').location.addressComponents;
        updateControls(addressComponents);
    },
    oninitialized: function(component) {
        var addressComponents = $(component).locationpicker('map').location.addressComponents;
        updateControls(addressComponents);
    },
    inputBinding: {
        latitudeInput: $('#us3-lat'),
        longitudeInput: $('#us3-lon'),
        radiusInput: $('#us3-radius'),
        locationNameInput: $('#us3-address')
    },
    enableAutocomplete: <?php if($order_type==1) { echo 'true'; } else { echo 'false';}?>,
    markerIcon: 'http://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi.png',
    //draggable: <?php if($order_type==1) { echo 'true'; } else { echo 'false';}?>,
    autocompleteOptions: {
        componentRestrictions: {country: 'fr'}
    },
    addressFormat: 'addressLine1',
    draggable: false,
    markerDraggable: false,
    markerVisible : true
});
$('#timelocation').on('shown.bs.modal', function () {
  $('#us3').locationpicker('autosize');
});
function updateControls(addressComponents) {
   // alert(addressComponents.addressLine1);
    $('.street1').val(addressComponents.addressLine1);
    $('.city').val(addressComponents.city);
    $('.state').val(addressComponents.stateOrProvince);
    $('.zip').val(addressComponents.postalCode);
    $('.country').val(addressComponents.country);

}


</script>



