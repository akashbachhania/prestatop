<style type="text/css">
/*.topping-option:first-child {
    margin-top: -7px;
}*/
.form-control:focus{
    border-color: <?php echo $button_info['button']['primary']['hover']?>!important;
    box-shadow:none!important;
}
.info_name{
    color: <?php echo $button_info['top_link']['color']?>;

}
.info i{
    color: <?php echo $button_info['button']['primary']['hover']?>;
    font-size: 20px;

}

.btn-circle.btn-lg {
  width: 50px;
  height: 50px;
  padding: 10px 16px;
  font-size: 18px;
  line-height: 1.33;
  border-radius: 25px;
}
.btn-circle.btn-xl {
  width: 70px;
  height: 70px;
  padding: 10px 16px;
  font-size: 24px;
  line-height: 1.33;
  border-radius: 35px;
}
.radio_btn {
    margin-top: 4px;
}
.btn-circle-check {
  border-radius: 0px!important;
}
.btn-circle {
    border-radius: 15px;
    font-size: 11px;
    height: 20px;
    line-height: 1.429;
    padding: 2px 0;
    text-align: center;
    width: 20px;
}
.check-items {
    line-height: 2;
    padding-top:5px; 
    padding-bottom:5px; 
}
.check-items:hover{
    padding-top:5px; 
    padding-bottom:5px; 
    background-color: #f7f7f7;
 
}
</style>



<?php $img_path=root_url().'assets/images/data/sprite-6b2bbb10c030ac8938a99c969bbb4fb5.png';?>
<style type="text/css">
    .modal.in .modal-dialog {
    transform: translate(0px, 0px);
}
.modal-close-button {
    border-radius: 50%;
    position: absolute;
    right: 0px;
    top: -5px;

}

.modal.fade .modal-dialog {
    transform: translate(0px, 0px);
    transition: transform 0.3s ease-out 0s;
}
.topping-modal .modal-dialog {
    margin-top: 70px;
    width: 600px;
}
.topping-modal .modal-dialog {
    margin-bottom: 0;
    margin-top: 0;
    width: 100%;
}
.toppings-product__name  b {
    line-height: initial!important;
    padding: initial!important;
    font-weight:700px;
}
.toppings-product__name b {
    font-family: "MuseoSans-700",Arial,sans-serif!important;
    font-weight:700px!important;
    font-size: 1.4rem!important;
    line-height: 1.5!important;
    padding: 15px 20px 0 15px!important;
}
.toppings-product__price b {
    line-height: initial!important;
    padding: initial!important;
}
.toppings-product__price b {
    font-family: "MuseoSans-700",Arial,sans-serif!important;
    font-weight:700px!important;
    font-size: 1.4rem!important;
    line-height: 1.41!important;
    padding: 0 15px!important;
}
.toppings-product__description {
    font-size: 1.4rem!important;
    line-height: 1.43!important;
    padding: 10px 0 15px!important;
}
.toppings-product__description {
    color: #666!important;
    font-size: 1.2rem!important;
    line-height: 1.41!important;
    padding: 0 15px 10px!important;
}
    .modal-footer::before, .modal-footer::after {
    content: " ";
    display: table;
}
.modal-footer::after {
    clear: both;
}
.modal-footer::before, .modal-footer::after {
    content: " ";
    display: table;
}
.topping-modal .modal-footer {
    padding: 0 20px 20px;
}
.topping-modal .modal-footer {
    border-top: medium none;
    padding: 15px;
}
.modal-footer {
    position: relative;
}
.modal-footer {
    border-top: 1px solid #e5e5e5;
    padding: 15px;
    text-align: right;
}
a.button {
    padding: 0;
}
.toppings-add__to__cart {
    float: left;
    min-height: 1px;
    padding-left: 10px;
    padding-right: 10px;
    position: relative;
    width: 50%;
}
.button, .button-secondary, .button-secondary--selected.charity__select-button:hover, .ios-smart-banner__view-app-link, .android-smart-banner__view-app-link, .button-secondary--no-hover {
    border: 0 none;
    border-radius: 4px;
    color: #fff;
    display: block;
    font-family: "MuseoSans-500",Arial,sans-serif;
    font-size: 1.4rem;
    height: 44px;
    padding: 0 20px;
    position: relative;
    text-align: center;
    text-transform: uppercase;
    transition: all 0.3s cubic-bezier(0.25, 0.46, 0.45, 0.94) 0s;
}
.pull-right, .toppings-add__to__cart {
    float: right !important;
}
.button__text {
    display: block;
    left: 0;
    padding: 0 20px;
/*    position: absolute;
    top: 50%;
    transform: translateY(-50%);*/
    width: 100%;
}
 
</style>
<style type="text/css">
    .toppings {
   /* border-top: 1px solid #bcbcbc;*/
}
.topping__header {
    font-size: 1.4rem;
    line-height: 1.9;
}
.topping__header {
    border-bottom: 1px solid #bcbcbc;
    cursor: pointer;
    font-family: "MuseoSans-700",Arial,sans-serif !important;
    font-size: 1.2rem;
    line-height: 2.13;
    margin: 0;
    overflow: auto;
    padding: 10px 0;
    text-transform: uppercase;
    transition: background-color 0.1s linear 0s;
}
.topping__header {
    font-weight: 700;
    font-size: 1.2rem;
    line-height: 2.13;
    border-bottom: 1px solid #ebebeb;
    padding: 8px 0;
    margin: 0;
    text-transform: uppercase;
    overflow: auto
}


.topping__header__name {
    padding-left: 0;
}
.topping__header__name {
    float: left;
    min-height: 1px;
    padding-left: 10px;
    padding-right: 10px;
    position: relative;
    width: 50%;
}
.topping__header__name { 
    position: relative;
    float: left;
    width: 50%;
    min-height: 1px;
    padding-left: 12px;
    padding-right: 12px;
}
.topping-option:first-child {
    margin-top: 15px;
}
.topping-option {
    padding: 10px 0;
}
.topping-option {
    color: #666;
    cursor: pointer;
    font-size: 1.4rem;
    line-height: 2;
    overflow: auto;
    padding: 10px 15px;
    transition: background-color 0.1s linear 0s;
}
.topping-option__button {
    background-image: url("<?php echo $img_path;?>");
    background-position: 0 -32px;
    display: inline-block;
    height: 18px;
    position: relative;
    top: 1px;
    vertical-align: text-top;
    width: 18px;
}
.selected .topping-option__button {
/*    background-image: url("<?php echo $img_path;?>");;
    background-position: 0 -86px;*/
    height: 18px;
    width: 18px;
}
.topping-option__button_checkbox {
    background-image: url("<?php echo $img_path;?>");
    background-position: 0 -104px;
    display: inline-block;
    height: 18px;
    position: relative;
    top: 1px;
    vertical-align: text-top;
    width: 18px;
}
.selected .topping-option__button_checkbox {
    background-image: url("<?php echo $img_path;?>");;
    background-position: 0 -14px;
    height: 18px;
    width: 18px;
}
.topping-option {
    color: #666;
    cursor: pointer;
    font-size: 1.4rem;
    line-height: 2;
}
 .topping__header__info {
    float: left;
    min-height: 1px;
    padding-left: 10px;
    padding-right: 0;
    position: relative;
    width: 50%;
}

.icon-up-open-big::before {
    content: "B";
}
[class^="icon-"]::before, [class*=" icon-"]::before {
    font-family: "icomoon" !important;
    font-style: normal;
    font-variant: normal;
    font-weight: normal;
    line-height: 1em;
    text-transform: none;
}
.topping__header__arrow {
    padding-right: 0;
}
.topping__header__arrow {
    color: <?php echo $button_info['button']['primary']['hover']?>;
    float: right;
    padding: 6px;
}
[class^="icon-"], [class*=" icon-"] {
    line-height: 1em;
}
.topping__header__comment {
    float: none;
    width: auto;
}
.topping__header__comment {
    color: <?php echo $button_info['button']['primary']['hover']?>;
    float: right;
    font-family: "MuseoSans-300",Arial,sans-serif;
    overflow: auto;
    padding-right: 0;
    text-transform: none;
    width: 80%;
}
.text-right, .home__stats__comment-minutes, .topping__header__comment {
    text-align: right;
}
.selection-required .topping__comment__help-text {
    color: #d70f64;
}
.topping__comment__help-text {
    color: #bcbcbc;
}

.icon-up-open-big::before {
    content: "B";
}
[class^="icon-"]::before, [class*=" icon-"]::before {
    font-family: "icomoon" !important;
    font-style: normal;
    font-variant: normal;
    font-weight: normal;
    line-height: 1em;
    text-transform: none;
}
.topping__header__arrow {
    padding-right: 0;
}
.topping__header__arrow {
    color: #d70f64;
    float: right;
    padding: 6px;
}
[class^="icon-"], [class*=" icon-"] {
    line-height: 1em;
}
.topping__options {
    border-bottom: 1px solid #bcbcbc;
    overflow: auto;
}

.topping-option:first-child {
    margin-top: 15px;
}
.topping-option {
    padding: 10px 0;
}
.topping-option {
    color: #666;
    cursor: pointer;
    font-size: 1.4rem;
    line-height: 2;
    overflow: auto;
    padding: 10px 15px;
    transition: background-color 0.1s linear 0s;
}
.topping-option__name {
    width: 95%;
}
.topping-option__name {
    float: left;
    padding-left: 0;
    width: 87%;
}
.close {
    margin-right: 23px;
    margin-top: 5px;
    width: 12px;
}
.lineheight2{
	line-height: 2.2;
}
.modal-content.noborderRadius{
	border-radius: 0px;
}
.fontsize20{
	font-size: 20px;
}
</style>

<div class="modal-dialog">
    <div class="modal-content noborderRadius">
       
        <div class="modal-body" id="menu-options<?php echo $menu_id; ?>">
            <div class="row">
                <div class="col-md-12">
                    <div class="modal-close-button " data-dismiss="modal">
                        <i class="fa fa-times"></i>
                    </div>
                        
                        <div class="toppings-product__name"><b><?php echo $menu_name; ?></b></div>
                        <?php if ($description) { ?>
                            <div class="toppings-product__price price"><b><?php echo $menu_price; ?></b></div>
                            <div class="toppings-product__description description"><?php echo $description; ?></div>
                        <?php } ?>

                        <div class="menu-options toppings">
                        	<input type="hidden" name="menu_id" value="<?php echo $menu_id; ?>" />
                        	<input type="hidden" name="row_id" value="<?php echo $row_id; ?>" />
                        	<?php if ($menu_options) { 
                            $count=$i=0;?>
                            <?php foreach ($menu_options as $key => $menu_option) { ?>
                                <?php if ($menu_option['display_type'] == 'radio') {?>
                                    <div class="option option-radio">
                                      <input type="hidden" name="menu_options[<?php echo $key; ?>][option_id]" value="<?php echo $menu_option['option_id']; ?>" />
                                      <input type="hidden" name="menu_options[<?php echo $key; ?>][menu_option_id]" value="<?php echo $menu_option['menu_option_id']; ?>" />

                                      <div class="toppings">
                                        <div class="topping selection-required optionsVisible">
                                          <!-- <div class="topping__header" id="<?php echo $i;?>">
                                            <div class="topping__header__name col-xs-9">
                                                <b><?php echo $menu_option['option_name'];?></b>
                                            </div> 
                                            <div class="pull-right col-xs-3">
                                              <center>
                                                <span class="info_name"><?php echo $menu_option['info'];?></span>&nbsp;&nbsp;
                                                <span class="info info_<?php echo $i;?>"><i class="fa fa-caret-<?php if($i==0)echo 'down';else echo 'up';?>">&nbsp;</i></span>
                                                    
                                                    </center>
                                            	</div> 
                                          </div> --> 
                                          <div class="topping__header expandable" id="<?php echo $i;?>">
										        <div class="topping__header__name">
										        		<?php echo $menu_option['option_name'];?>
								        		</div>
										        <div class="topping__header__info">
										            <div class="topping__header__arrow icon-down-open-big info_<?php echo $i;?>">
										            	<i class="fa fa-chevron-<?php if($i==0)echo 'down';else echo 'up';?>">&nbsp;</i>
										            </div>
										            <div class="topping__header__comment">
										                <div class="topping__comment__help-text">
										                    <?php echo $menu_option['info'];?>                
									                  </div>
										                <!-- <div class="topping__comment__special-instructions <% if (special_instructions === '') { %>hide<% } %>">
										                    <%- special_instructions %>
										                </div> -->
										            </div>
										        </div>
										    </div>
                                        </div>
                                      </div>
                                    </div>
                                    
                                    <div style="<?php if($i==0){ echo ''; } else { echo 'display:none'; } ?>" class="toggleAll toggle_<?php echo $i;?>">    
                                      <div class="topping__options"  <?php if($count!=0) echo 'style=""';?>>
                                        <div class="topping-option">
                                          <?php if (isset($menu_option['option_values'])) { ?>
                                            <?php foreach ($menu_option['option_values'] as $option_value) {
                                            	  
                                            	
                                             ?>
                                              <?php isset($cart_option_value_ids[$key]) OR $cart_option_value_ids[$key] = array() ?>
                                                <?php if (in_array($option_value['menu_option_value_id'], $cart_option_value_ids[$key]) OR (empty($cart_option_value_ids[$key]) AND $menu_option['default_value_id'] == $option_value['menu_option_value_id'])) { ?>
                                                    <div class="selected myradie_class_<?php echo $option_value['option_value_id']; ?>" >
                                                      <div class="row check-items radio-option__button" id="<?php echo $option_value['option_value_id'];?>" dd="<?php echo $key;?>">
                                                        <div class="col-xs-10">
                                                          <?php echo $option_value['value']; ?>
                                                          <?php if(filter_var($option_value['price'], FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION) !== "0.00"){ echo '(+'.$option_value['price'].')'; }
                                                          ?>
                                                        </div>
                                                        <div class="col-xs-1">
                                                          <input type="radio" style="display:none"  class="myradie_<?php echo $option_value['option_value_id']; ?>" name="menu_options[<?php echo $key; ?>][option_values][]" value="<?php echo $option_value['option_value_id']; ?>" checked="checked" />

                                                          <button type="button" class="radio-option__button1 btn btn-primary btn-circle  myradie_<?php echo $option_value['option_value_id']; ?> rad_<?php echo $key;?>"  style="border-radius: 15px;">
                                                          	<i class="fa fa-check" aria-hidden="true"></i>
                                                          </button>
                                                        
                                                        </div>
                                                      </div>
                                                    </div>
                                                  <?php } else { ?>
                                                    <div class="row check-items radio-option__button" id="<?php echo $option_value['option_value_id'];?>" dd="<?php echo $key;?>"> 
                                                      	<div class="col-xs-10">
                                                          <?php echo $option_value['value']; ?>
                                                         	<?php if(filter_var($option_value['price'], FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION) !== "0.00"){ echo '(+'.$option_value['price'].')'; } ?>
                                                      	</div>
                                                      	<div class="col-xs-1">
                                                        	<input type="radio" style="display:none" class="myradie_<?php echo $option_value['option_value_id']; ?>" name="menu_options[<?php echo $key; ?>][option_values][]" value="<?php echo $option_value['option_value_id'];?>" >
                                                          <button type="button" class="radio-option__button1 btn btn-default btn-circle  myradie_<?php echo $option_value['option_value_id'];?> rad_<?php echo $key;?>" style="border-radius: 15px;">
                                                          </button>

                                                        </div>
                                                      </div>
                                                    <?php } ?>
                                                <?php  } ?>
                                            <?php } ?>
                                            </div>
                                        </div>
                                    </div>   
                                <?php $count++; } ?>
                                <?php if ($menu_option['display_type'] == 'checkbox') { ?>
                                  <div class="menu-options" style="display: block;">
                                    <input type="hidden" name="menu_options[<?php echo $key; ?>][option_id]" value="<?php echo $menu_option['option_id']; ?>" />
                                    <input type="hidden" name="menu_options[<?php echo $key; ?>][menu_option_id]" value="<?php echo $menu_option['menu_option_id']; ?>" />

                                    <div class="option option-radio">
                                      <input type="hidden" name="menu_options[<?php echo $key; ?>][option_id]" value="<?php echo $menu_option['option_id']; ?>" />
                                      <input type="hidden" name="menu_options[<?php echo $key; ?>][menu_option_id]" value="<?php echo $menu_option['menu_option_id']; ?>" />
                                      <div class="toppings">
                                        <div class="topping selection-required optionsVisible">
                                         
                                             <div class="topping__header expandable" id="<?php echo $i;?>">
												        <div class="topping__header__name">
												        		<?php echo $menu_option['option_name'];?>
										        		</div>
												        <div class="topping__header__info">
												            <div class="topping__header__arrow icon-down-open-big info_<?php echo $i;?>"><i class="fa fa-chevron-<?php if($i==0)echo 'down';else echo 'up';?>">&nbsp;</i></div>
												            <div class="topping__header__comment">
												                <div class="topping__comment__help-text">
												                    <?php echo $menu_option['info'];?>                
											                  </div>
												                <!-- <div class="topping__comment__special-instructions <% if (special_instructions === '') { %>hide<% } %>">
												                    <%- special_instructions %>
												                </div> -->
												            </div>
												        </div>
												    </div>              
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  
                                  <div  style="<?php if($i==0){ echo ''; } else { echo 'display:none'; } ?>" class="toggleAll toggle_<?php echo $i;?>">
                                    <div class="topping__options" style="">
                                        <div class="topping-option">
                                    <?php if (isset($menu_option['option_values'])) {  ?>
                                      <?php foreach ($menu_option['option_values'] as $option_value) { 
                                      	?>
                                        <?php isset($cart_option_value_ids[$key]) OR $cart_option_value_ids[$key] = array() ?>
                                          <?php if (in_array($option_value['menu_option_value_id'], $cart_option_value_ids[$key]) OR (empty($cart_option_value_ids[$key]) AND $menu_option['default_value_id'] == $option_value['menu_option_value_id'])) { ?>
                                              <div class="row check-items check-option__button" id="<?php echo $option_value['option_value_id'];?>">
                                                <div class="col-xs-10">
                                                 	<?php echo $option_value['value'];?>
                                                 	<?php if(filter_var($option_value['price'], FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION) != "0.00"){ echo '(+'.$option_value['price'].')'; } ?>
                                                </div>   
                                                <div class="col-xs-1">
                                                  <input style="display:none" type="checkbox" class="mycheckbox_<?php echo $option_value['option_value_id'];?>" name="menu_options[<?php echo $key; ?>][option_values][]" value="<?php echo $option_value['option_value_id']; ?>" checked="checked"/>
                                                  <button type="button" class="btn btn-primary btn-circle  btn-circle-check mycheckbox_<?php echo $option_value['option_value_id']; ?>" id="<?php echo $option_value['option_value_id'];?>">
                                                    <i class="fa fa-check" aria-hidden="true"></i>
                                                  </button>
                                                        
                                              	</div> 
                                              </div>  
                                            <?php } else { ?>
                                              <div class="row check-items check-option__button" id="<?php echo $option_value['option_value_id'];?>">
                                                  <div class="col-xs-10">
                                                 		<?php echo $option_value['value'];?>
                                                   	<?php if(filter_var($option_value['price'], FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION) != "0.00"){ echo '(+'.$option_value['price'].')'; } ?>
                                                  </div>   
                                                  <div class="col-xs-1">
                                                    <input style="display:none" type="checkbox" class="mycheckbox_<?php echo $option_value['option_value_id'];?>" name="menu_options[<?php echo $key; ?>][option_values][]" value="<?php echo $option_value['option_value_id']; ?>" />
                                                  
                                                    <button type="button" class="btn btn-default btn-circle  btn-circle-check mycheckbox_<?php echo $option_value['option_value_id']; ?>" id="<?php echo $option_value['option_value_id'];?>">
                                                    </button>
                                                  
                                                  </div>   
                                                </div>
                                              <?php } ?>
                                            <?php  } ?>
                                        <?php } ?>
                                    </div>
                                </div>
                                     	</div>
                                    </div>
                                <?php } $i++; } } ?>
                            </div>
                    				<div class="clearfix"></div>
            				<div class="modal-footer">
                        			<?php if ($row_id) { ?>
                            		<a class="toppings-add__to__cart btn btn-primary lineheight2" onclick="addToCart('<?php echo $menu_id; ?>');" title="<?php echo lang('text_update'); ?>">
                                	<span class="button__text">
                                    <?php echo lang('button_update'); ?>
                                	</span>
                            		</a>
                        			<?php } else { ?>
                            		<a class="toppings-add__to__cart btn btn-primary lineheight2" onclick="addToCart('<?php echo $menu_id; ?>');" title="<?php echo lang('text_add_to_order'); ?>">
                                 	<span class="button__text">   
                                  	<?php echo lang('button_add_to_order'); ?>
                                	</span>    
                            		</a>
                        			<?php } ?>
                    <div class="input-group quantity-control col-xs-5">
                        <span class="input-group-btn ">
                          <button class="btn btn-primary lineheight2" data-dir="dwn" type="button"><i class="fa fa-minus"></i></button>
                        </span>
                        <input type="text" name="quantity" id="quantity" class="form-control text-center fontsize20" value="<?php echo $quantity; ?>">
                        <span class="input-group-btn ">
                            <button class="btn btn-primary lineheight2" data-dir="up" type="button"><i class="fa fa-plus"></i></button>
                        </span>
                	</div>
                    <div class="clearfix"></div>
                    <br>
                    <div id="cart-options-alert">
                        <?php if ($cart_option_alert) { ?>
                            <?php echo $cart_option_alert; ?>
                        <?php } ?>
                    </div>
				</div>
			</div>
        </div>
    </div>
</div>
<script type="text/javascript"><!--
$(document).ready(function() {
    //$('.option-select select.form-control').selectpicker({showSubtext:true});

    $('.quantity-control .btn').on('click', function() {
        var $button = $(this);
        var oldValue = $button.parent().parent().find('#quantity').val();

        if ($button.attr('data-dir') == 'up') {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            var newVal = (oldValue > 0) ? parseFloat(oldValue) - 1 : 0;
        }

        $button.parent().parent().find('#quantity').val(newVal);
    });
});
//--></script>

<script>
$(document).on('click','.modal-close-button',function() { 
    $('#optionsModal').modal('hide');
});
</script>

<script>


$('.radio-option__button').click(function() { 
    $('.rad_'+$(this).attr('dd')).html("");
   
    $('.rad_'+$(this).attr('dd')).removeClass('btn-primary');
    $('.rad_'+$(this).attr('dd')).removeClass('btn-default');
    $('.rad_'+$(this).attr('dd')).addClass('btn-default');
    $('.myradie_'+this.id).prop("checked", true);
    $('.myradie_'+this.id).addClass('btn-default');
    $('.myradie_'+this.id).addClass('btn-primary');
    $('.myradie_'+this.id).html("<i class='fa fa-check' aria-hidden='true'></i>");

});

$('.check-option__button').click(function() { 
        console.log("checkbox call");
    if($('.mycheckbox_'+this.id).prop("checked") == true){
        console.log("if");
        $('.mycheckbox_'+this.id).prop( "checked", false );
        $('.mycheckbox_'+this.id).text('');
        $('.mycheckbox_'+this.id).addClass('btn-default');
        $('.mycheckbox_'+this.id).removeClass('btn-primary');
    }
    else 
    {
        console.log("else");
        $('.mycheckbox_'+this.id).prop( "checked", true);
        $('.mycheckbox_'+this.id).html("<i class='fa fa-check' aria-hidden='true'></i>");
        $('.mycheckbox_'+this.id).removeClass('btn-default');
        $('.mycheckbox_'+this.id).addClass('btn-primary');

    }
});
$('.topping__header').click(function() { 
	// $('.topping__header__arrow').html('<i class="fa fa-chevron-down">&nbsp;</i>');
    //
    var display = $('.toggle_'+this.id).css('display'); 
    $(".toggleAll").hide();
	// $('.toggle_'+this.id).toggle();
     
     if ( display == 'none' ){
        $('.toggle_'+this.id).show();
    }
    else if(display == 'block'){
      $('.toggle_'+this.id).hide();  
    }
    
    $(this).closest('.toggleAll').siblings().hide();
	// $('.info_'+this.id).html("<i class='fa  fa-chevron-up'>&nbsp;</i>");
	$(this).find('i').toggleClass('fa-chevron-up fa-chevron-down');
});


</script>
