<?php $img_path=root_url().'assets/images/data/sprite-6b2bbb10c030ac8938a99c969bbb4fb5.png';?>
<style type="text/css">
    .modal.in .modal-dialog {
    transform: translate(0px, 0px);
}
.modal-close-button {
    background-image: url("<?php echo $img_path?>");
    background-position: 0 -151px;
    font-size: 21px;
    cursor: pointer;
    height: 31px;
    position: absolute;
    right: 4px;
    top: 2px;
    width: 31px;
}

.modal.fade .modal-dialog {
    transform: translate(0px, 0px);
    transition: transform 0.3s ease-out 0s;
}
.topping-modal .modal-dialog {
    margin-top: 70px;
    width: 600px;
}
.topping-modal .modal-dialog {
    margin-bottom: 0;
    margin-top: 0;
    width: 100%;
}
.toppings-product__name  b {
    line-height: initial!important;
    padding: initial!important;
    font-weight:700px;
}
.toppings-product__name b {
    font-family: "MuseoSans-700",Arial,sans-serif!important;
    font-weight:700px!important;
    font-size: 1.4rem!important;
    line-height: 1.5!important;
    padding: 15px 20px 0 15px!important;
}
.toppings-product__price b {
    line-height: initial!important;
    padding: initial!important;
}
.toppings-product__price b {
    font-family: "MuseoSans-700",Arial,sans-serif!important;
    font-weight:700px!important;
    font-size: 1.4rem!important;
    line-height: 1.41!important;
    padding: 0 15px!important;
}
.toppings-product__description {
    font-size: 1.4rem!important;
    line-height: 1.43!important;
    padding: 10px 0 15px!important;
}
.toppings-product__description {
    color: #666!important;
    font-size: 1.2rem!important;
    line-height: 1.41!important;
    padding: 0 15px 10px!important;
}
    modal-footer::before, .modal-footer::after {
    content: " ";
    display: table;
}
.modal-footer::after {
    clear: both;
}
.modal-footer::before, .modal-footer::after {
    content: " ";
    display: table;
}
.topping-modal .modal-footer {
    padding: 0 20px 20px;
}
.topping-modal .modal-footer {
    border-top: medium none;
    padding: 15px;
}
.modal-footer {
    position: relative;
}
.modal-footer {
    border-top: 1px solid #e5e5e5;
    padding: 15px;
    text-align: right;
}
a.button {
    padding: 0;
}
.toppings-add__to__cart {
    float: left;
    min-height: 1px;
    padding-left: 10px;
    padding-right: 10px;
    position: relative;
    width: 50%;
}
.button, .button-secondary, .button-secondary--selected.charity__select-button:hover, .ios-smart-banner__view-app-link, .android-smart-banner__view-app-link, .button-secondary--no-hover {
    background-color: #d70f64;
    border: 0 none;
    border-radius: 4px;
    color: #fff;
    display: block;
    font-family: "MuseoSans-500",Arial,sans-serif;
    font-size: 1.4rem;
    height: 44px;
    padding: 0 20px;
    position: relative;
    text-align: center;
    text-transform: uppercase;
    transition: all 0.3s cubic-bezier(0.25, 0.46, 0.45, 0.94) 0s;
}
.pull-right, .toppings-add__to__cart {
    float: right !important;
}
.button__text {
    display: block;
    left: 0;
    padding: 0 20px;
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    width: 100%;
}
    .btn-default, .panel-default .panel-heading, .panel-nav-tabs.panel-default .panel-heading {
     background-color: #f7f7f7!important;
    color: #d70f64!important;
    cursor: pointer;
    font-size: 1.2rem;
    line-height: inherit;
    text-align: center
}
</style>
<style type="text/css">
    .toppings {
    border-top: 1px solid #bcbcbc;
}
.topping__header {
    font-size: 1.4rem;
    line-height: 1.9;
}
.topping__header {
    border-bottom: 1px solid #bcbcbc;
    cursor: pointer;
    font-family: "MuseoSans-700",Arial,sans-serif !important;
    font-size: 1.2rem;
    line-height: 2.13;
    margin: 0;
    overflow: auto;
    padding: 10px 0;
    text-transform: uppercase;
    transition: background-color 0.1s linear 0s;
}
.topping__header__name {
    padding-left: 0;
}
.topping__header__name {
    float: left;
    min-height: 1px;
    padding-left: 10px;
    padding-right: 10px;
    position: relative;
    width: 50%;
}
.topping-option:first-child {
    margin-top: 15px;
}
.topping-option {
    padding: 10px 0;
}
.topping-option {
    color: #666;
    cursor: pointer;
    font-size: 1.4rem;
    line-height: 2;
    overflow: auto;
    padding: 10px 15px;
    transition: background-color 0.1s linear 0s;
}
.topping-option__button {
    background-image: url("<?php echo $img_path;?>");
    background-position: 0 -32px;
    display: inline-block;
    height: 18px;
    position: relative;
    top: 1px;
    vertical-align: text-top;
    width: 18px;
}
.selected .topping-option__button {
    background-image: url("<?php echo $img_path;?>");;
    background-position: 0 -86px;
    height: 18px;
    width: 18px;
}
.topping-option__button_checkbox {
    background-image: url("<?php echo $img_path;?>");
    background-position: 0 -104px;
    display: inline-block;
    height: 18px;
    position: relative;
    top: 1px;
    vertical-align: text-top;
    width: 18px;
}
.selected .topping-option__button_checkbox {
    background-image: url("<?php echo $img_path;?>");;
    background-position: 0 -14px;
    height: 18px;
    width: 18px;
}
.topping-option {
    color: #666;
    cursor: pointer;
    font-size: 1.4rem;
    line-height: 2;
}
 .topping__header__info {
    float: left;
    min-height: 1px;
    padding-left: 10px;
    padding-right: 0;
    position: relative;
    width: 50%;
}

.icon-up-open-big::before {
    content: "B";
}
[class^="icon-"]::before, [class*=" icon-"]::before {
    font-family: "icomoon" !important;
    font-style: normal;
    font-variant: normal;
    font-weight: normal;
    line-height: 1em;
    text-transform: none;
}
.topping__header__arrow {
    padding-right: 0;
}
.topping__header__arrow {
    color: #d70f64;
    float: right;
    padding: 6px;
}
[class^="icon-"], [class*=" icon-"] {
    line-height: 1em;
}
.topping__header__comment {
    float: none;
    width: auto;
}
.topping__header__comment {
    color: #292929;
    float: right;
    font-family: "MuseoSans-300",Arial,sans-serif;
    overflow: auto;
    padding-right: 0;
    text-transform: none;
    width: 80%;
}
.text-right, .home__stats__comment-minutes, .topping__header__comment {
    text-align: right;
}
.selection-required .topping__comment__help-text {
    color: #d70f64;
}
.topping__comment__help-text {
    color: #bcbcbc;
}

.icon-up-open-big::before {
    content: "B";
}
[class^="icon-"]::before, [class*=" icon-"]::before {
    font-family: "icomoon" !important;
    font-style: normal;
    font-variant: normal;
    font-weight: normal;
    line-height: 1em;
    text-transform: none;
}
.topping__header__arrow {
    padding-right: 0;
}
.topping__header__arrow {
    color: #d70f64;
    float: right;
    padding: 6px;
}
[class^="icon-"], [class*=" icon-"] {
    line-height: 1em;
}
.topping__options {
    border-bottom: 1px solid #bcbcbc;
    overflow: auto;
}

.topping-option:first-child {
    margin-top: 15px;
}
.topping-option {
    padding: 10px 0;
}
.topping-option {
    color: #666;
    cursor: pointer;
    font-size: 1.4rem;
    line-height: 2;
    overflow: auto;
    padding: 10px 15px;
    transition: background-color 0.1s linear 0s;
}
.topping-option__name {
    width: 95%;
}
.topping-option__name {
    float: left;
    padding-left: 0;
    width: 87%;
}
.modal-close-button {
    right: 15px;
    top: -16px;
}
.close {
    margin-right: 23px;
    margin-top: 5px;
    width: 12px;
}
</style>


<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4>confirm your address</h4>
            <div class="modal-close-button" data-dismiss="modal">&nbsp;</div>
        </div>
        <div class="modal-body" id="timelocation">
            <div class="row">

                <div class="col-md-12">
                <form class="" role="form"  accept-charset="utf-8">
                    <div class="form-group ">
                        <input type="text" placeholder="Enter your full address" name="time_location" id="time_location1" autocomplete="on" class="form-control input-lg">

                    </div>










                <div class="modal-footer">

                    <a class="toppings-add__to__cart button active btn-block savelocation" onclick="" title="">
                         <span class="button__text">   
                           Save
                        </span>    
                    </a>
                </div>
        
                </form>
                    
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript"><!--
$(document).on('click','.modal-close-button',function() { 
    $('#timelocation').modal('hide');
});
</script>



