<?php //echo '<pre>';print_r($has_delivery); die("Df");?>


<style type="text/css">
    .pac-container {
        z-index: 10000;
    }
</style>
<div class="<?php echo ($is_mobile OR $is_checkout) ? '' : 'hidden-xs'; ?>" <?php echo $fixed_cart; ?>>
    <div id="cart-box" class="module-box">
        <div class="panel panel-default panel-cart <?php echo ($is_checkout) ? 'hidden-xs' : ''; ?>">
            <div class="panel-heading">
                <h3 class="panel-title"><?php echo lang('text_heading'); ?></h3>
            </div>

            <?php if (!empty($cart_alert)) { ?>
            <div class="panel-body">
                <div id="cart-alert" class="cart-alert-wrap">
                    <div class="cart-alert"></div>
                        <?php echo $cart_alert; ?>
                </div>
            </div>
            <?php } ?>
            <?php if (!empty(lang('text_card_message'))) { ?>
            <div class="panel-body">
                <p class="text-center"><?php echo lang('text_card_message'); ?></p>
            </div>
            <?php } ?>

                <?php if ($has_delivery OR $has_collection) { ?>
                    <div class="location-control text-center text-muted">
                        <div id="my-postcode" style="display:<?php echo (empty($alert_no_postcode)) ? 'block' : 'none'; ?>">
                            <div class="btn-group btn-group-md text-center order-type" data-toggle="buttons">
                                <?php if ($has_delivery) { ?>
                                    <label onClick="opentimelocation(1);" class=" btn <?php echo ($order_type === '1') ? 'btn-default btn-primary active' : 'btn-default'; ?>" data-btn="btn-primary">
                                        <input type="radio" name="order_type" value="1" <?php echo ($order_type === '1') ? 'checked="checked"' : ''; ?>>&nbsp;&nbsp;<strong><?php echo lang('text_delivery'); ?></strong>
                                        <span class="small center-block">
                                            <?php if ($delivery_status === 'open') { ?>
                                                <?php echo sprintf(lang('text_in_minutes'), $delivery_time); ?>
                                            <?php } else if ($delivery_status === 'opening') { ?>
                                                <?php echo sprintf(lang('text_starts'), $delivery_time); ?>
                                            <?php } else { ?>
                                                <?php echo lang('text_is_closed'); ?>
                                            <?php } ?>
                                        </span>
                                    </label>
                                <?php } ?>
                                <?php if ($has_collection) { ?>
                                    <label onClick="opentimelocation(2);" class="btn <?php echo ($order_type === '2') ? 'btn-default btn-primary active' : 'btn-default'; ?>" data-btn="btn-primary">
                                        <input type="radio" name="order_type" value="2" <?php echo ($order_type === '2') ? 'checked="checked"' : ''; ?>>&nbsp;&nbsp;<strong><?php echo lang('text_collection'); ?></strong>
                                        <span class="small center-block">
                                            <?php if ($collection_status === 'open') { ?>
                                                <?php echo sprintf(lang('text_in_minutes'), $collection_time); ?>
                                            <?php } else if ($collection_status === 'opening') { ?>
                                                <?php echo sprintf(lang('text_starts'), $collection_time); ?>
                                            <?php } else { ?>
                                                <?php echo lang('text_is_closed'); ?>
                                            <?php } ?>
                                        </span>
                                    </label>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <div id="cart-info">
                    <?php if ($cart_items) {?>
                        <div class="cart-items">
                            <ul>
                                <?php foreach ($cart_items as $cart_item) { ?>
                                    <li>
                                        <a class="cart-btn remove text-muted small" onClick="removeCart('<?php echo $cart_item['menu_id']; ?>', '<?php echo $cart_item['rowid']; ?>', '0');"><i class="fa fa-minus-circle"></i></a>
                                        <a class="name-image" onClick="openMenuOptions('<?php echo $cart_item['menu_id']; ?>', '<?php echo $cart_item['rowid']; ?>');">
                                            <?php if (!empty($cart_item['image'])) { ?>
                                                <img class="image img-responsive img-thumbnail" width="<?php echo $cart_images_w; ?>" height="<?php echo $cart_images_h; ?>" alt="<?php echo $cart_item['name']; ?>" src="<?php echo $cart_item['image']; ?>">
                                            <?php } ?>
                                            <span class="name">
                                                <span class="quantity"><?php echo $cart_item['qty'].lang('text_times'); ?></span>
                                                <?php echo $cart_item['name']; ?>
                                            </span>
                                            <?php if (!empty($cart_item['options'])) { ?>
                                                <span class="options text-muted small"><?php echo $cart_item['options']; ?></span>
                                            <?php } ?>
                                        </a>
                                        <p class="comment-amount">
                                            <span class="amount pull-right"><?php echo $cart_item['sub_total']; ?></span>
                                            <?php if (!empty($cart_item['comment'])) { ?>
                                                <span class="comment text-muted small">[<?php echo $cart_item['comment']; ?>]</span>
                                            <?php } ?>
                                        </p>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>

                        <div class="cart-coupon">
                            <div class="input-group">
                                <input type="text" name="coupon_code" class="form-control" value="<?php echo isset($coupon['code']) ? $coupon['code'] : ''; ?>" placeholder="<?php echo lang('text_apply_coupon'); ?>" />
                                <span class="input-group-btn"><a class="btn btn-default" onclick="applyCoupon();" title="<?php echo lang('button_apply_coupon'); ?>"><i class="fa fa-check"></i></a></span>
                            </div>
                        </div>

                        <div class="cart-total">
                            <div class="table-responsive">
                                <table width="100%" height="auto" class="table table-none">
                                    <tbody>
                                        <?php foreach ($cart_totals as $name => $total) { ?>
                                            <?php if (!empty($total)) { ?>
                                                <tr>
                                                    <td><span class="text-muted">
                                                        <?php if ($name === 'order_total') { ?>
                                                            <b><?php echo $total['title']; ?>:</b>
                                                        <?php } else if ($name === 'coupon' AND isset($total['code'])) { ?>
                                                            <?php echo $total['title']; ?>:&nbsp;&nbsp;
                                                            <a class="remove clickable" onclick="clearCoupon('<?php echo $total['code']; ?>');"><span class="fa fa-times"></span></a>
                                                        <?php } else { ?>
                                                            <?php echo $total['title']; ?>:
                                                        <?php } ?>
                                                    </span></td>
                                                    <td class="text-right">
                                                        <?php if ($name === 'coupon') { ?>
                                                            -<?php echo $total['amount']; ?>
                                                        <?php } else if ($name === 'order_total') { ?>
                                                            <b><span class="order-total"><?php echo $total['amount']; ?></span></b>
                                                        <?php } else { ?>
                                                            <?php echo $total['amount']; ?>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="panel-body"><?php echo lang('text_no_cart_items'); ?></div>
                    <?php } ?>
                </div>
            </div>
        </div>

        <?php if (!empty($button_order)) { ?>
            <div class="cart-buttons wrap-none">
                <div class="center-block">
                    <?php echo $button_order; ?>
                    <?php if (!$is_mobile) { ?>
                        <a class="btn btn-link btn-block visible-xs" href="<?php echo site_url('cart') ?>"><?php echo lang('button_view_cart'); ?></a>
                    <?php } ?>
                </div>
                <div class="clearfix"></div>
            </div>
        <?php } ?>
    </div>
</div>
<?php
if ($this->config->item('maps_api_key')) {
        $map_key = '&key=' . $this->config->item('maps_api_key');
    } else {
        $map_key = '';
    }
if($this->uri->segment(1)=='checkout') {
?>
<script src="https://maps.googleapis.com/maps/api/js?<?php echo $map_key;?>&libraries=places&ver=2.1.1"></script>
<?php } ?>
<div id="cart-buttons" class="<?php echo (!$is_mobile AND !$is_checkout) ? 'visible-xs' : 'hide'; ?>">
    <a class="btn btn-default cart-toggle" href="<?php echo site_url('cart') ?>" style="text-overflow:ellipsis; overflow:hidden;">
        <?php echo lang('text_heading'); ?>
        <span class="order-total"><?php echo (!empty($order_total)) ? '&nbsp;&nbsp;-&nbsp;&nbsp;'.$order_total : ''; ?></span>
    </a>
</div>
<?php if (!$is_mobile) { ?>
<div class="cart-alert-wrap cart-alert-affix visible-xs-block"><div class="cart-alert"></div><?php if (!empty($cart_alert)) { echo $cart_alert; } ?></div>
<?php } ?>
<script type="text/javascript"><!--
    var alert_close = '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';

    var cartHeight = pageHeight-(65/100*pageHeight);

    $(document).on('ready', function() {
        $('.cart-alert-wrap .alert').fadeTo('slow', 0.1).fadeTo('slow', 1.0).delay(5000).slideUp('slow');
        $('#cart-info .cart-items').css({"height" : "auto", "max-height" : cartHeight, "overflow" : "auto", "margin-right" : "-15px", "padding-right" : "5px"});

        $(window).bind("load resize", function() {
            var sideBarWidth = $('#content-right .side-bar').width();
            $('#cart-box-affix').css('width', sideBarWidth);
        });
    });

    $(document).on('change', 'input[name="order_type"]', function() { console.log("jj");
        if (typeof this.value !== 'undefined') {
            var order_type = this.value;
            console.log(order_type);
            $.ajax({
                url: js_site_url('cart_module/cart_module/order_type'),
                type: 'post',
                data: 'order_type=' + order_type,
                dataType: 'json',
                success: function (json) {
                    if (json['redirect'] && json['order_type'] == order_type) {
                       // window.location.href = json['redirect'];
                    }
                }
            });
        }
    });

    function addToCart(menu_id, quantity) {
        if ($('#menu-options' + menu_id).length) {
            var data = $('#menu-options' + menu_id + ' input:checked, #menu-options' + menu_id + ' input[type="hidden"], #menu-options' + menu_id + ' select, #menu-options' + menu_id + ' textarea, #menu-options' + menu_id + '  input[type="text"]');
        } else {
            var data = 'menu_id=' + menu_id + '&quantity=' + quantity;
        }

        $('#menu'+menu_id+ ' .add_cart').removeClass('failed');
        $('#menu'+menu_id+ ' .add_cart').removeClass('added');
        if (!$('#menu'+menu_id+ ' .add_cart').hasClass('loading')) {
            $('#menu'+menu_id+ ' .add_cart').addClass('loading');
        }

        $.ajax({
            url: js_site_url('cart_module/cart_module/add'),
            type: 'post',
            data: data,
            dataType: 'json',
            success: function(json) {
                $('#menu'+menu_id+ ' .add_cart').removeClass('loading');
                $('#menu'+menu_id+ ' .add_cart').removeClass('failed');
                $('#menu'+menu_id+ ' .add_cart').removeClass('added');

                if (json['option_error']) {
                    $('#cart-options-alert .alert').remove();

                    $('#cart-options-alert').append('<div class="alert" style="display: none;">' + alert_close + json['option_error'] + '</div>');
                    $('#cart-options-alert .alert').fadeIn('slow');

                    $('#menu' + menu_id + ' .add_cart').addClass('failed');
                } else {
                    $('#optionsModal').modal('hide');

                    if (json['error']) {
                        //$('#menu' + menu_id + ' .add_cart').addClass('failed');
                    }

                    if (json['success']) {
                        //$('#menu' + menu_id + ' .add_cart').addClass('added');
                       // $('#menu' + menu_id + ' .add_cart').remove();
                        $('#menu'+menu_id+' .menu__item__wrapper .menu-content .menu__item__name .shopping').html('<i class="fa fa-shopping-cart"></i>');
                    }

                    updateCartBox(json);
                }
            }
        });
    }

    function openMenuOptions(menu_id, row_id) {
        if (menu_id) {
            var row_id = (row_id) ? row_id : '';

            $.ajax({
                url: js_site_url('cart_module/cart_module/options?menu_id=' + menu_id + '&row_id=' + row_id),
                dataType: 'html',
                success: function(html) {

                    $('#optionsModal').remove();
                    $('body').append('<div id="optionsModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>');
                    $('#optionsModal').html(html);

                    $('#optionsModal').modal();
                    $('#optionsModal').on('hidden.bs.modal', function(e) {
                        $('#optionsModal').remove();
                    });
/*                    $('#menu'+menu_id+' .menu__item__wrapper .menu__item__price a').remove();
                    $('#menu'+menu_id+' .menu__item__wrapper .menu-content .shopping').html('<i class="fa fa-shopping-cart"></i>');
*/
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
    }
    function removeCart(menu_id, row_id, quantity) {
        $.ajax({
            url: js_site_url('cart_module/cart_module/remove'),
            type: 'post',
            data: 'menu_id' + menu_id + '&row_id=' + row_id + '&quantity=' + quantity,
            dataType: 'json',
            success: function(json) {
            $('#menu'+menu_id+' .menu__item__wrapper .menu-content .menu__item__name .shopping .fa-shopping-cart').remove();
            //$('#menu'+menu_id+' .menu__item__wrapper  .menu__item__price .add_plus').html('<a class="menu__item__price-button btn-cart add_cart"><i class="menu__item__price-button-icon icon-plus"></i></a>');
                updateCartBox(json)
            }
        });
    }

    function applyCoupon() {
        var coupon_code = $('#cart-box input[name="coupon_code"]').val();
        $.ajax({
            url: js_site_url('cart_module/cart_module/coupon'),
            type: 'post',
            data: 'action=add&code=' + coupon_code,
            dataType: 'json',
            success: function(json) {
                updateCartBox(json)
            }
        });
    }

    function clearCoupon(coupon_code) {
        $('input[name=\'coupon\']').attr('value', '');

        $.ajax({
            url: js_site_url('cart_module/cart_module/coupon'),
            type: 'post',
            data: 'action=remove&code=' + coupon_code,
            dataType: 'json',
            success: function(json) {
                updateCartBox(json)
            }
        });
    }

    function updateCartBox(json) {
        var alert_message = '';

        if (json['redirect']) {
            window.location.href = json['redirect'];
        }

        if (json['error']) {
            alert_message = '<div class="alert">' + alert_close + json['error'] + '</div>';
            updateCartAlert(alert_message);
        } else {
            if (json['success']) {
                alert_message = '<div class="alert">' + alert_close + json['success'] + '</div>';
            }

            $('#cart-box').load(js_site_url('cart_module/cart_module #cart-box > *'), function(response) {
                updateCartAlert(alert_message);
            });
        }
    }

    function updateCartAlert(alert_message) {
        if (alert_message != '') {
            $('.cart-alert-wrap .alert, .cart-alert-wrap .cart-alert').empty();
            $('.cart-alert-wrap .cart-alert').append(alert_message);
            $('.cart-alert-wrap .alert').slideDown('slow').fadeTo('slow', 0.1).fadeTo('slow', 1.0).delay(5000).slideUp('slow');
        }

        if ($('#cart-info .order-total').length > 0) {
            $('#cart-box-affix .navbar-toggle .order-total').html(" - " + $('#cart-info .order-total').html());
        }

        $('#cart-info .cart-items').css({"height" : "auto", "max-height" : cartHeight, "overflow" : "auto", "margin-right" : "-15px", "padding-right" : "5px"});
    }
//function for time and location deepak

/*function initialize1() { 
var options = {
    types: ['geocode'],
    componentRestrictions: {country: "fr"}
};

var input = document.getElementById('time_location1');
var autocomplete = new google.maps.places.Autocomplete(input, options);
}
google.maps.event.addDomListener(window, 'load', initialize1);
*/
      // This example displays an address form, using the autocomplete feature
      // of the Google Places API to help users fill in the information.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      var placeSearch, autocomplete;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

   google.maps.event.addDomListener(window, 'load', initAutocomplete);


var myCenter=new google.maps.LatLng(51.508742,-0.120850);

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            (document.getElementById('autocomplete')),
            {
                types: ['geocode']
               ,componentRestrictions: {country: "fr"}

        });







//its working code
        var mapProp = {
          center: myCenter,
          zoom:8,
          mapTypeId: google.maps.MapTypeId.ROADMAP
          };

        var map = new google.maps.Map(document.getElementById("dvMap"),mapProp);

        var marker = new google.maps.Marker({
          position: myCenter,
          title:'Click to zoom'
          });

        marker.setMap(map);

        // Zoom to 9 when clicking on marker
        google.maps.event.addListener(marker,'click',function() {
          map.setZoom(16);
          map.setCenter(marker.getPosition());
          });
             
        google.maps.event.addListener(map,'center_changed',function() {
        // 3 seconds after the center of the map has changed, pan back to the marker
          window.setTimeout(function() {
            map.panTo(marker.getPosition());
          },3000);
          });



        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);





      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }
   




    function opentimelocation(order_type) {

            $.ajax({
            url: js_site_url('cart_module/cart_module/citylocation'),
            type: 'post',
            data:'order_type=' + order_type,
                success: function(html) {
                    $('#timelocation').remove();
                    $('body').append('<div id="timelocation" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>');
                    $('#timelocation').html(html);

                    $('#timelocation').modal();
                    $('#timelocation').on('hidden.bs.modal', function(e) {
                        $('#timelocation').remove();
                    });
                    initAutocomplete();

                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }

            });
    }


$(document).on('click','.savelocation',function() { 
var pageURL = $(location).attr("href");
pageURL=pageURL.substr(pageURL.lastIndexOf('/') + 1)

//alert(pageURL);
if($('#street_number').val()=='')
{
$('.addressvalidation').show();

$('.addressvalidation span').html('Street address is not present').show();

}else if($('.day_select :selected').val()=='')
{
$('.addressvalidation').show();

$('.addressvalidation span').html('Places Select day').show();
}
else{
var postdata  ='address_1=' +  $('#street_number').val() + '&address_2=' + $('#route').val() + '&city=' + $('#locality').val() + '&state=' + $('#administrative_area_level_1').val() + '&postcode=' + $('#postal_code').val()+ '&country=' + $('#country').val() +'&delivery_day=' + $('.delivery_day').val() + '&delivery_time=' + $('.delivery_time').val()+ '&location=' + $('.location').val();
$('.addressvalidation').hide();
    $.ajax({
        url: js_site_url('cart_module/cart_module/setcitylocation'),
        type: 'post',
        data:postdata,
        success: function(html) { 
        //    alert('success');
        $('.timelocation1').modal('hide');
        $('#timelocation').modal('hide');
            if(pageURL=='checkout')
            {
                location.reload();
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
}
/*$('#timelocation').hide();
    $.ajax({
//        url: js_site_url('cart_module/cart_module/options?menu_id=' + menu_id + '&row_id=' + row_id),
        url: js_site_url('cart_module/cart_module/confurmcitylocationcon'),
        dataType: 'html',
        success: function(html) {
            $('#timelocation').remove();
            $('body').append('<div id="timelocation" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>');
            $('#timelocation').html(html);

            $('#timelocation').modal();
            $('#timelocation').on('hidden.bs.modal', function(e) {
                $('#timelocation').remove();
            });
            initialize1();



        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }

    });*/
});



    //--></script>

