<?php $img_path=root_url().'assets/images/logo.png';?>


<style type="text/css">
	.menu__categories__list-wrapper {
    padding-top: 10px;
}
li {
    list-style: outside none none;
}
.menu__categories__list-item {
/*    color: #292929!important;*/
    cursor: pointer!important;
    font-size: 14px!important;
    transition: color 0.1s linear 0s!important;
}
.menu__categories__list-item:hover,.menu__categories__list-item:focus {
/*    color: #d70f64!important;*/
    text-decoration:none!important;
}

#category-box .panel {
    padding-right: 0!important;
}
/*.partial .module-box li, .partial > .module-box .list-group-item, #category-box .list-group-item a, .cart-items ul li {
    background-color: transparent!important;
    border-color: #eeeeee!important;
    border-style: none!important;
	font-family: "MuseoSans-800",Arial,sans-serif!important;

}*/
a, .link {
/*    color: #d70f64;*/
    cursor: pointer;
    transition: color 0.1s linear 0s;
}
a {
/*    color: #337ab7;*/
    text-decoration: none!important;
}
a {
/*    background-color: transparent;*/
}
.menu__categories {
    float: left;
    width: 25%;
}
.menu__categories {
    min-height: 1px;
    padding-left: 10px;
    padding-right: 10px;
    position: relative;
    text-align: right;
}
/*body {
    color: #292929!important;
	font-family: "MuseoSans-300",Arial,sans-serif!important;
    font-size: 1.6rem!important;
    font-weight: 400!important;
    line-height: 1.6!important;
}*/
.menu__categories__vendor-logo {
    margin: 0;
padding: 0px 0px 0px 20px;
}
</style>
<div id="category-box-affix" <?php echo $fixed_categories; ?>>
	<div id="category-box " class="module-box">
        <div class="menu__categories__vendor-logo">
            <img alt="logo" src="<?php echo $img_path?>" class="img-responsive">
        </div>
		<div class="panel panel-default navdiv" align="right">
			<?php echo $category_tree; ?>
		</div>
	</div>
</div>
<script type="text/javascript"><!--
	$(document).ready(function() {
		$('#category-box-collapse .list-group-item').on('click', function() {
			if ($('#category-box-collapse.in').length > 0) $('#category-box-collapse').collapse('toggle');
		});

		$(window).bind("load resize", function() {
			var sideBarWidth = $('#content-left .side-bar').width();
			$('#category-box-affix').css('width', sideBarWidth);
		});
	});
//-->
</script>


<script type="text/javascript">
	
$('a[href^="#"]').click(function(e) {
 e.preventDefault();	
 var asd = $('.'+this.id);
 $('html,body').animate({ scrollTop: asd.offset().top-50}, 600);
     return false;
});

</script>